#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT
script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

if !hash sudo >/dev/null 2>&1
then
    echo "Sudo not installed!!"
fi

sudo pacman -S --needed ansible openssh git base-devel go

mkdir -p $HOME/devel/open_source

cd $HOME/devel/open_source

rm -rf ansible-yay
git clone https://github.com/mnussbaum/ansible-yay.git

mkdir -p $HOME/.ansible/plugins/modules/

cd ansible-yay

cp yay $HOME/.ansible/plugins/modules/
