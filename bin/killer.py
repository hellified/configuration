#!/usr/bin/env  python3
import psutil
import signal

for proc in psutil.process_iter():
    try:
        pinfo = proc.as_dict(attrs=['pid', 'name','exe','cmdline'])
    except psutil.NoSuchProcess:
        pass
    else:
        if (pinfo["cmdline"] != None):
            if "sbt" in ','.join(pinfo["cmdline"]):
                print(pinfo)
                proc.send_signal(signal.SIGKILL)
            else:
                pass
