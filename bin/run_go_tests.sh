#!/bin/bash -ex

function find_go_test_dirs {
    test_dirs_found=$(find . -name '*_test.go' -printf '%h\n' | sort -u)
}

declare -a test_dirs
test_dirs=($test_dirs_found)

for test_dir in "${test_dirs[@]}"; do
    echo "Test dir: [${test_dir}]"
    (
        cd $test_dir
        go test -v -json | jq
    )
done
                
