#!/bin/bash -e
cd /home/isahod/devel/kambi/git/data_science/feature-generator-scheduled-event-start/
eval "$(direnv export bash)"
./runner.sh config.ini $(date -I -d yesterday)
