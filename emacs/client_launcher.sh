#!/bin/bash
ARG=${1##emacs://}
LINE=${ARG##*/}
FILE=${ARG%/*}
ARG="-c"
emacsclient $ARG -n +$LINE "$FILE"
exit $?