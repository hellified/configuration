;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Hellfied's Emacs Environment
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Suppressing message: "Package cl is deprecated"

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq byte-compile-warnings '(cl-functions))

(progn (cd "~/.emacs.d")
       (normal-top-level-add-subdirs-to-load-path))

(progn (cd "~"))

(add-to-list 'load-path "~/.emacs.d/lisp")
;; (load-library "hellified-kambi")

(use-package "exec-path-from-shell")
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

(load-library "hellified-functions")
;; Ensure all my desired packages are in place
(load-library "hellified-packages")

(load-library "hellified-emacs-client")
(load-library "hellified-faces")
(load-library "hellified-modeline")
(load-library "hellified-key-bindings")
(load-library "hellified-misc")
(load-library "hellified-org")
(load-library "hellified-term")
(load-library "hellified-tramp")

;; General programming
;;(load-library "hellified-treesit")
;;(load-library "hellified-lsp")
(load-library "hellified-programming")
(load-library "hellified-magit")
(load-library "hellified-company")
(load-library "hellified-treemacs")
(load-library "hellified-markdown")
(load-library "hellified-cloud")
(load-library "hellified-docker")
(load-library "hellified-webdev")

(load-library "hellified-yasnippet")
;;(load-library "hellified-abbrev")

;; Language specific
;;(load-library "hellified-go-ts")
;;(load-library "hellified-go-lsp")
(load-library "hellified-go")
(load-library "hellified-java")
;;(load-library "hellified-javascript")

;; Writing
(when (string= system-name "corvus")
  (string= system-name "anorexia")
  (load-library "hellified-writing")
  )

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-preview ((t (:foreground "darkgray" :underline t))))
 '(company-preview-common ((t (:inherit company-preview))))
 '(company-preview-search ((t (:inherit company-preview))))
 '(company-template-field ((t (:background "magenta" :foreground "black"))))
 '(company-tooltip ((t (:background "lightgray" :foreground "black"))))
 '(company-tooltip-annotation ((t (:background "white smoke" :foreground "black"))))
 '(company-tooltip-annotation-selection ((t (:background "color-253"))))
 '(company-tooltip-common ((((type x)) (:inherit company-tooltip :weight bold)) (t (:inherit company-tooltip))))
 '(company-tooltip-common-selection ((((type x)) (:inherit company-tooltip-selection :weight bold)) (t (:inherit company-tooltip-selection))))
 '(company-tooltip-mouse ((t (:foreground "black"))))
 '(company-tooltip-scrollbar-thumb ((t (:background "red"))))
 '(company-tooltip-scrollbar-track ((t (:background "brightwhite"))))
 '(company-tooltip-search ((t (:background "white smoke" :foreground "black"))))
 '(company-tooltip-selection ((t (:background "steelblue" :foreground "white")))))
(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["black" "red" "pale green" "slate grey" "dark salmon" "forest green" "white"])
 '(connection-local-criteria-alist
   '(((:application tramp :protocol "flatpak")
      tramp-container-connection-local-default-flatpak-profile)
     ((:application tramp)
      tramp-connection-local-default-system-profile tramp-connection-local-default-shell-profile)))
 '(connection-local-profile-alist
   '((tramp-container-connection-local-default-flatpak-profile
      (tramp-remote-path "/app/bin" tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin" "/usr/local/bin" "/usr/local/sbin" "/local/bin" "/local/freeware/bin" "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin" "/opt/bin" "/opt/sbin" "/opt/local/bin"))
     (tramp-connection-local-darwin-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o" "pid,uid,user,gid,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "state=abcde" "-o" "ppid,pgid,sess,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etime,pcpu,pmem,args")
      (tramp-process-attributes-ps-format
       (pid . number)
       (euid . number)
       (user . string)
       (egid . number)
       (comm . 52)
       (state . 5)
       (ppid . number)
       (pgrp . number)
       (sess . number)
       (ttname . string)
       (tpgid . number)
       (minflt . number)
       (majflt . number)
       (time . tramp-ps-time)
       (pri . number)
       (nice . number)
       (vsize . number)
       (rss . number)
       (etime . tramp-ps-time)
       (pcpu . number)
       (pmem . number)
       (args)))
     (tramp-connection-local-busybox-ps-profile
      (tramp-process-attributes-ps-args "-o" "pid,user,group,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "stat=abcde" "-o" "ppid,pgid,tty,time,nice,etime,args")
      (tramp-process-attributes-ps-format
       (pid . number)
       (user . string)
       (group . string)
       (comm . 52)
       (state . 5)
       (ppid . number)
       (pgrp . number)
       (ttname . string)
       (time . tramp-ps-time)
       (nice . number)
       (etime . tramp-ps-time)
       (args)))
     (tramp-connection-local-bsd-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o" "pid,euid,user,egid,egroup,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" "-o" "state,ppid,pgid,sid,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etimes,pcpu,pmem,args")
      (tramp-process-attributes-ps-format
       (pid . number)
       (euid . number)
       (user . string)
       (egid . number)
       (group . string)
       (comm . 52)
       (state . string)
       (ppid . number)
       (pgrp . number)
       (sess . number)
       (ttname . string)
       (tpgid . number)
       (minflt . number)
       (majflt . number)
       (time . tramp-ps-time)
       (pri . number)
       (nice . number)
       (vsize . number)
       (rss . number)
       (etime . number)
       (pcpu . number)
       (pmem . number)
       (args)))
     (tramp-connection-local-default-shell-profile
      (shell-file-name . "/bin/sh")
      (shell-command-switch . "-c"))
     (tramp-connection-local-default-system-profile
      (path-separator . ":")
      (null-device . "/dev/null"))))
 '(custom-enabled-themes '(material))
 '(custom-safe-themes
   '("90a6f96a4665a6a56e36dec873a15cbedf761c51ec08dd993d6604e32dd45940" "7922b14d8971cce37ddb5e487dbc18da5444c47f766178e5a4e72f90437c0711" "b9e9ba5aeedcc5ba8be99f1cc9301f6679912910ff92fdf7980929c2fc83ab4d" "84d2f9eeb3f82d619ca4bfffe5f157282f4779732f48a5ac1484d94d5ff5b279" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" default))
 '(package-selected-packages
   '(latex-math-preview latex-preview-pane crontab-mode dotenv-mode exec-path-from-shell helm-lsp helm-lisp lsp-java templ-ts-mode company go-projectile flymake-go go-flymake auto-complete corfu treemacs yasnippet-snippets which-key web-mode wc-mode visual-fill-column use-package uniquify-files treemacs-tab-bar treemacs-projectile treemacs-persp treemacs-magit treemacs-icons-dired tramp-term tramp-auto-auth terraform-mode systemd smart-tabs-mode smart-mode-line-powerline-theme rainbow-delimiters protobuf-mode plantuml-mode org-wc ob-graphql nanowrimo material-theme markdown-preview-mode lsp-ui lsp-tailwindcss lsp-ivy js2-highlight-vars hl-todo hl-sentence go-guru go friendly-tramp-path flymake-json flymake-jslint flycheck-inline flycheck-gometalinter dockerfile-mode docker-compose-mode direnv dap-mode company-go ack))
 '(warning-suppress-types '((comp))))
