(clear-abbrev-table global-abbrev-table)

;; define abbrev for specific major mode
;; the first part of the name should be the value of the variable major-mode of that mode
;; e.g. for go-mode, name should be go-mode-abbrev-table

;; (when (boundp 'jinja2-mode-abbrev-table)
;;   (clear-abbrev-table jinja2-mode-abbrev-table))

;; (define-abbrev-table 'jinja2-mode-abbrev-table
;;   '(
;;     ("html" "<html>
;; </html>")

;;      ("head" "<head>
;; </head>")

;;      ("h1" "<h1> </h1>")

;;      ("body" "<body>
;; </body>")

;;      ("zt" "<title> </title>")
;;      ("p" "<p> </p>")
;;      ("div" "<div>
;; </div>")

;;      ("<a" "<a href=\"\"> </a>")



;;      )
;;   )


(set-default 'abbrev-mode t)

(setq save-abbrevs nil)
