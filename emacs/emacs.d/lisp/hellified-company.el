;; ############################################
;; BEGIN: company
;; ############################################
;; auto-complete
;; (package-install 'company)
;; (global-company-mode)

;; (use-package company-lsp)
;; (push 'company-lsp company-backends)
;; (setq company-minimum-prefix-length 1
;;       company-idle-delay 0.2) ;; default is 0.2
;; ############################################
;; END: company
;; ############################################
