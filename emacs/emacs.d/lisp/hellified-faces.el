(defun trim-string (string)
  "Remove white spaces in beginning and ending of STRING.
White space here is any of: space, tab, emacs newline (line feed, ASCII 10)."
(replace-regexp-in-string "\\`[ \t\n]*" "" (replace-regexp-in-string "[ \t\n]*\\'" "" string))
)
(defun empty-string-p (string)
  "Return true if the string is empty or nil. Expects string."
  (or (null string)
      (zerop (length (trim-string string)))))

(when (not (empty-string-p (getenv "SSH_TTY")) )
  (set-face-attribute 'default nil :font "-ADBO-Source Code Pro-normal-normal-normal-*-35-*-*-*-m-0-iso10646-1" ))

;; Runme byte compilation file
;; find . -name "*.el" | awk '{print "(byte-compile-file \"" $1 "\")";}' > runme.el
;; emacs -batch -l runme.el -kill

(use-package ansi-color)
(setq ansi-color-names-vector
      ["black" "black" "black" "black"
       "black" "black" "black" "black"])
(ansi-color-map-update 'ansi-color-names-vector ansi-color-names-vector)
(when (display-graphic-p)
  (set-background-color "gray86")
  (scroll-bar-mode 0)
  (tool-bar-mode 0)
  (menu-bar-mode 0))
;;NavajoWhite PaleGoldenrod NavajoWhite2
(when (string= system-name "corvus")
  (set-face-attribute 'default nil :font "-ADBO-Source Code Pro-normal-normal-normal-*-36-*-*-*-m-0-iso10646-1" ))
(when (string= system-name "anorexia")
  (set-face-attribute 'default nil :font "-ADBO-Source Code Pro-normal-normal-normal-*-36-*-*-*-m-0-iso10646-1" ))
;; Modeline: mode-line-format
;; ("%e" mode-line-front-space mode-line-mule-info mode-line-client mode-line-modified mode-line-remote mode-line-frame-identification mode-line-buffer-identification "   " mode-line-position
;;  (vc-mode vc-mode)
;;  "  " mode-line-modes mode-line-misc-info mode-line-end-spaces)
