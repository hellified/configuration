(use-package "go-mode")
(use-package "lsp-mode"
  :hook (
         (go-mode . lsp)
         )
  )
(use-package lsp-ui)

;; Company mode
(use-package "company")
(setq company-idle-delay 0)
(setq company-minimum-prefix-length 1)

(defun lsp-go-install-save-hooks ()
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)

;; Start LSP Mode and YASnippet mode
(add-hook 'go-mode-hook #'lsp-deferred)
(add-hook 'go-mode-hook #'yas-minor-mode)

(defun go-mode-setup ()
  (subword-mode 1) ;; Support CamelCase
  (company-mode 1)
  (add-to-list 'write-file-functions 'delete-trailing-whitespace)
  (setq compile-command "make clean build")
  (define-key (current-local-map) "\C-c\C-c" 'compile)
  (go-eldoc-setup))

(add-hook 'go-mode-hook 'go-mode-setup)

(use-package "go-projectile")
(setq go-projectile-tools
  '((gocode    . "github.com/mdempsky/gocode")
    (golint    . "golang.org/x/lint/golint")
    (godef     . "github.com/rogpeppe/godef")
    (errcheck  . "github.com/kisielk/errcheck")
    (godoc     . "golang.org/x/tools/cmd/godoc")
    (gogetdoc  . "github.com/zmb3/gogetdoc")
    (goimports . "golang.org/x/tools/cmd/goimports")
    (gorename  . "golang.org/x/tools/cmd/gorename")
    (gomvpkg   . "golang.org/x/tools/cmd/gomvpkg")
    (guru      . "golang.org/x/tools/cmd/guru")))


;; ############################################
;; BEGIN: linting
;; ############################################
(use-package flycheck-gometalinter
  :ensure t
  :config
  (progn
    (flycheck-gometalinter-setup)))
;; Only enable selected linters
(setq flycheck-gometalinter-disable-all t)
(setq flycheck-gometalinter-enable-linters '("golangci-lint"))
;; skips 'vendor' directories and sets GO15VENDOREXPERIMENT=1
(setq flycheck-gometalinter-vendor t)
;; only show errors
;;(setq flycheck-gometalinter-errors-only t)
;; only run fast linters
;; (setq flycheck-gometalinter-fast t)
;; use in tests files
(setq flycheck-gometalinter-tests t)
;; Set different deadline (default: 5s)
(setq flycheck-gometalinter-deadline "5s")
;; Use a gometalinter configuration file (default: nil)
;;(setq flycheck-gometalinter-config "/path/to/gometalinter-config.json")
;; ############################################
;; END: linting
;; ############################################
