(use-package projectile)
(use-package flycheck)
(use-package yasnippet :config (yas-global-mode))
(use-package lsp-mode :hook ((lsp-mode . lsp-enable-which-key-integration))
  :config (setq lsp-completion-enable-additional-text-edit nil))
(use-package hydra)
(use-package company)
(use-package lsp-ui)
(use-package which-key :config (which-key-mode))
(use-package lsp-java :config (add-hook 'java-mode-hook 'lsp))
(use-package dap-mode :after lsp-mode :config (dap-auto-configure-mode))
(use-package dap-java :ensure nil)
;; (use-package helm-lsp)
;; (use-package helm
;;   :config (helm-mode))
(use-package lsp-treemacs)

;; Compiling code
;; Add color formatting to *compilation* buffer
(add-hook 'compilation-filter-hook
   (lambda () (ansi-color-apply-on-region (point-min) (point-max))))

(setq compilation-scroll-output 'first-error)

(defun my-recompile ()
    "Run compile and resize the compile window closing the old one if necessary"
    (interactive)
    (progn
      (if (get-buffer "*compilation*") ; If old compile window exists
  	(progn
  	  (delete-windows-on (get-buffer "*compilation*")) ; Delete the compilation windows
  	  (kill-buffer "*compilation*") ; and kill the buffers
  	  )
        )
      (call-interactively 'projectile-compile-project)
      )
  )
  (defun my-next-error ()
    "Move point to next error and highlight it"
    (interactive)
    (progn
      (next-error)
      (end-of-line-nomark)
      (beginning-of-line-mark)
      )
  )

  (defun my-previous-error ()
    "Move point to previous error and highlight it"
    (interactive)
    (progn
      (previous-error)
      (end-of-line-nomark)
      (beginning-of-line-mark)
      )
    )

(defun in-directory (dir)
  "Runs execute-extended-command with default-directory set to the given
directory."
  (interactive "In directory: ")
  (let ((default-directory dir))
    (call-interactively 'execute-extended-command)))

(add-hook 'java-mode-hook
          (lambda ()
            (subword-mode 1)
            (setq c-basic-offset 2)
            )
          )

;; (progn
;;   (print (lsp-java--ls-command)))
