(use-package js2-mode
  :mode "\\.js\\'"
  :hook (js2-mode . lsp-deferred)
  )

;; (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;; Hook it in for shell scripts running via node.js:
(add-to-list 'interpreter-mode-alist '("node" . js2-mode))
(add-hook 'js2-mode-hook
          (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

;; (use-package smart-tabs-mode)
;; (smart-tabs-insinuate 'javascript)
;; (smart-tabs-advice js2-indent-line js2-basic-offset)
