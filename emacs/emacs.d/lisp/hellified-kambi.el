;; Kambi proxy settings needed for package fetching
(when (string= system-name "KMB-5CG8172J8N")
  (setq url-proxy-services '(("http" . "http://isahod:@proxy.stho.kambi.com:3128"))))


(define-generic-mode story-mode
  '("!-")
  '("GivenStories" "Given" "When" "Then" "Narrative" "Meta" "And" "Scenario" "Examples")
  nil
  nil
  nil
  "Story mode is a minor mode for editing JBehave story files")

(add-to-list 'auto-mode-alist '("\\.story" . story-mode))
(use-package tramp-hdfs)
