;;################################
;;Global generic key bindings
;;################################
(global-set-key [f2] `copy-region-as-kill)
(global-set-key [f3] 'yas-reload-all)
(global-set-key [f4] `kill-region)

(global-set-key [f5] 'hs-toggle-hiding)
(global-set-key [f6] 'display-line-numbers-mode)
;; press F7 on keypad to lookup definition
(use-package dictionary)
(global-set-key [f7] 'dictionary-search)
(global-set-key [f8] 'recentf-open-files)

(global-set-key [f9] 'flycheck-next-error)
(global-set-key [f10] 'flycheck-list-errors)
;; [f11] leave for full screen toggle
(global-set-key [f12] 'count-words)
