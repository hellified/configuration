(use-package latex-math-preview)

(defun latex-mode-setup ()
  (define-key (current-local-map) "C-c e" 'latex-math-preview-expression)
  )

(add-hook 'latex-mode-hook 'latex-mode-setup)

