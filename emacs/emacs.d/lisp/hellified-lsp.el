(use-package lsp-mode
  :init
  :ensure t
  :commands (lsp lsp-mode lsp-deferred)
  :hook (
         (go-mode . lsp)
         lsp-enable-which-key-integration
         )
  :config  (setq lsp-prefer-flymake nil
                 lsp-enable-indentation nil
                 lsp-enable-on-type-formatting nil)
  (setq lsp-keymap-prefix "s-l")
  (lsp-modeline-code-actions-mode)
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (add-to-list 'lsp-file-watch-ignored "\\.~\\'")
  )

(use-package lsp-ui :commands lsp-ui-mode)
(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)
(lsp-treemacs-sync-mode 1)


(with-eval-after-load 'lsp-mode
  ;; Don't watch vendor directories
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\.vendor\\'")
  ;; Don't watch tilde files
  (add-to-list 'lsp-file-watch-ignored "\\.~\\'")
  )
