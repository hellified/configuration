;; UTF-8 as default encoding
(set-language-environment "UTF-8")
(prefer-coding-system 'utf-8)
(setq coding-system-for-read 'utf-8)
(setq coding-system-for-write 'utf-8)

(setq visible-bell 1)

(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

(setq-default user-mail-address "isaac+tech@bridgefire.net")

;; move the mouse pointer to the corner of the screen when approached
;; http://www.emacswiki.org/emacs/MouseAvoidance
(mouse-avoidance-mode 'exile)

;; Create better buffer names. foo.c++:src, foo.c++:extra instead of foo.c++ and foo.c++<2>
;; This should really be the default behaviour for Emacs!
(use-package uniquify-files)
(setq
 uniquify-buffer-name-style 'reverse
 uniquify-separator ":")

;; save place in a file
(save-place-mode 1)

;; make all "yes or no" prompts show "y or n" instead
(fset 'yes-or-no-p 'y-or-n-p)

;;##################################
;;Delete region when typing
;;##################################
(delete-selection-mode 1)
(put 'downcase-region 'disabled nil)

(setq-default kill-whole-line t)
(setq-default scroll-bar-mode (quote right))
(setq-default transient-mark-mode t)

(use-package visual-fill-column)
(setq-default fill-column 90)
;;(add-hook 'visual-line-mode-hook #'visual-fill-column-mode)


(show-paren-mode t)

;Reload .emacs on the fly
(defun reload-dot-emacs()
  (interactive)
  (if(bufferp (get-file-buffer ".emacs.d/init.el"))
      (save-buffer(get-buffer ".emacs.d/init.el")))
  (load-file "~/.emacs.d/init.el")
  (message "init.el reloaded successfully"))

;;Place all backup copies of files in a common location
(defconst use-backup-dir t)
(setq backup-directory-alist (quote ((".*" . "~/.emacs-meta/backups/")))
      version-control t ; Use version numbers for backups
      kept-new-versions 16 ; Number of newest versions to keep
      kept-old-versions 2 ; Number of oldest versions to keep
      delete-old-versions t ; Ask to delete excess backup versions?
      backup-by-copying-when-linked t) ; Copy linked files, don't rename.

;;Allow fetching files from HTTP servers
(url-handler-mode)

(use-package ack)

;;Recent files
(use-package recentf)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)


(defun just-one-space-in-region (beg end)
  "replace all whitespace in the region with single spaces"
  (interactive "r")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (while (re-search-forward "\\s-+" nil t)
        (replace-match " ")))))

;; minor mode for Emacs that displays the key bindings following your currently entered incomplete command
(use-package which-key)

(use-package systemd)

;;Reload file if changed on disk
(global-auto-revert-mode t)

;;; hellified-misc.el ends here
