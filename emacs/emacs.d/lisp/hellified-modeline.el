(setq-default column-number-mode t)

;; 24 hour time
(setq-default display-time-24hr-format 1)
(display-time)


(setq-default size-indication-mode t)

(use-package smart-mode-line :ensure t)
(use-package smart-mode-line-powerline-theme
  :ensure t
  :after smart-mode-line
  :init
  (progn (setq sml/no-confirm-load-theme t))
  :config
  (sml/setup)
  (sml/apply-theme 'smart-mode-line-light-powerline))


(use-package rich-minority
  :ensure t
  :after smart-mode-line
  :config
  (setq rm-blacklist
        (format "^ \\(%s\\)$"
                (mapconcat #'identity
                           '("FlyC.*"
                             "Flymake.*"
                             "LSP.*"
                             "Projectile.*"
                             "WS"
                             "Helm"
                             "Paredit"
			                       "yas")
                           "\\|")))
  (rich-minority-mode 1))
