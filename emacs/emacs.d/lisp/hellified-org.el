(use-package remember)
(setq org-log-done 'time)

(setf org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))

;; Per-file setting syntax
;;#+SEQ_TODO: TODO(t) NEXT(n) DONE(d)
(setq org-todo-keywords
      '((sequence "TODO"
		          "WORKING"
		          "|"
		          "DONE" "DELEGATED")))

(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
              ("NEXT" :foreground "blue" :weight bold)
              ("WORKING" :foreground "darkblue" :weight bold)
              ("PULL_REQUEST" :foreground "orange" :weight bold)
              ("NEEDS_RELEASE" :foreground "magenta" :weight bold)
              ("RELEASING" :foreground "light green" :weight bold)
              ("DONE" :foreground "forest green" :weight bold)
              ("DELEGATED" :foreground "forest green" :weight bold)
	      )
	     )
      )

      ;; '((sequence "TODO"
      ;;             "WORKING"
      ;;             "DONE_LOCAL"
      ;;             "IN_REVIEW"
      ;;             "NEEDS_RELEASE"
      ;;             "RELEASING"
      ;;             "NEEDS_DEB_BUILD"
      ;;             "BUILDING_DEB"
      ;;             "NEEDS_CID_PUSH"
      ;;             "CID_PUSH"
      ;;             "|"
      ;;             "DONE"
      ;;             "DELEGATED")))
