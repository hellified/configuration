;;; Package -- Python configuration for hellified
;;; Commentary:

;;; Code:

(setq py-python-command "python3")

;; ###############################################################
;; BEGIN: elpy
;; ###############################################################
;; (use-package elpy
;;   :ensure t
;;   :init
;;   (elpy-enable))

;; (setq elpy-rpc-python-command "python3")
;; (add-hook 'elpy-mode-hook (lambda ()
;;                             (highlight-indentation-mode -1)
;;                             ))

;; (add-to-list 'process-coding-system-alist '("elpy" . (utf-8 . utf-8)))

;; (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
;; (add-hook 'elpy-mode-hook 'flycheck-mode)
;; ;; python mode keybindings
;; (define-key python-mode-map (kbd "M-.") 'jedi:goto-definition)
;; (define-key python-mode-map (kbd "M-,") 'jedi:goto-definition-pop-marker)
;; (define-key python-mode-map (kbd "M-/") 'jedi:show-doc)
;; (define-key python-mode-map (kbd "M-?") 'helm-jedi-related-names)
;; ;; end python mode keybindings
;; ###############################################################
;; END: elpy
;; ###############################################################

(use-package poetry)
(use-package blacken)
(setq blacken-only-if-project-is-blackened 1)
;; (use-package py-autopep8
;;   :config
;;   ;;(setq py-autopep8-options '("--ignore=E501,W293,W391,W690"))
;;   (add-hook 'python-mode-hook 'py-autopep8-enable-on-save))

;; (use-package py-isort)
;; (add-hook 'before-save-hook 'py-isort-before-save)


(setenv "IPY_TEST_SIMPLE_PROMPT" "1")
(setq python-shell-interpreter "python3"
      python-shell-interpreter-args "-i")

(use-package highlight-indent-guides)

(add-hook 'python-mode-hook (lambda ()
                              (subword-mode 1)
                              (setq-default visual-line-mode 1)
                              (setq-default highlight-indent-guides-mode 1)
                              ))
(setq highlight-indent-guides-method 'character)

(add-hook 'python-mode-hook
          (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

;; completion
(use-package company-jedi)
(defun my/python-mode-hook ()
  (add-to-list 'company-backends 'company-jedi))

(add-hook 'python-mode-hook 'my/python-mode-hook)

;; ###############################################################
;; Pipfile editing
;; ###############################################################
(use-package pip-requirements)
(setq auto-mode-alist
      (append
       '(("Pipfile" . pip-requirements-mode))
       auto-mode-alist))

(add-hook 'pip-requirements-mode-hook #'pip-requirements-auto-complete-setup)

;; ###############################################################
;; iPython environment
;; ###############################################################
(use-package ein)
(setq python-shell-interpreter "ipython")
(setq python-shell-interpreter-args "-i --simple-prompt --InteractiveShell.display_page=True")

(use-package jinja2-mode)

(fset 'jinja-translate
   (kmacro-lambda-form [f4 ?\{ ?\{ ?  ?_ ?\( ?\' ?\C-y ?\' ?\) ?  ?\} ?\}] 0 "%d"))
(define-key jinja2-mode-map (kbd "s--") 'jinja-translate)

(setenv "PYTHONIOENCODING" "utf-8")
(add-to-list 'process-coding-system-alist '("python" . (utf-8 . utf-8)))
(add-to-list 'process-coding-system-alist '("flake8" . (utf-8 . utf-8)))
;;; hellified-python.el ends here
