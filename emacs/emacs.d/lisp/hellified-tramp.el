(setq tramp-default-method "ssh")
(use-package friendly-tramp-path
  :after tramp)
(use-package tramp-auto-auth)
(use-package tramp-term)
