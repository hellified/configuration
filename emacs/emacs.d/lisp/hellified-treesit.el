(use-package tree-sitter)
(use-package tree-sitter-langs)

(global-tree-sitter-mode)

(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)

(setq treesit-language-source-alist
      '((go "https://github.com/tree-sitter/tree-sitter-go""v0.19.1" )
        (gomod "https://github.com/camdencheek/tree-sitter-go-mod")))

(setq treesit-font-lock-level 4)
