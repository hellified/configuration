(use-package wc-mode)

(setq abbrev-file-name             ;; tell emacs where to read abbrev
      "~/.emacs.d/abbrev_defs")    ;; definitions from...

;;Abbrev mode always on
(setq save-abbrevs t)
;; (setq-default abbrev-mode t)

;;Tie abbrev mode to edit mode
;; (dolist (hook '(erc-mode-hook
;;                     emacs-lisp-mode-hook
;;                     text-mode-hook))
;;       (add-hook hook (lambda () (abbrev-mode 1))))
(add-to-list 'auto-minor-mode-alist '("\\.txt\\'" . wc-mode))

;; check spelling
(add-hook 'text-mode-hook 'flyspell-mode)
(add-hook 'org-mode-hook 'flyspell-mode)
(setq ispell-dictionary "en_US"
      ispell-extra-args '() ;; TeX mode "-t"
      ispell-silently-savep t)
(setq ispell-program-name "/usr/bin/hunspell")
(setq ispell-personal-dictionary "~/.emacs.d/personal/hunspell-dict") ;; add personal dictionary

;; (use-package langtool)
;; (setq langtool-language-tool-jar "~/java/LanguageTool-2.8/languagetool-commandline.jar")
;; (setq langtool-mother-tongue "en")

(defun write-mode ()
  (interactive)
  (hl-sentence-mode)
  (variable-pitch-mode)
  (nanowrimo-mode))

(use-package org-wc)
(use-package nanowrimo)
(setq nanowrimo-today-goal 500)

(use-package hl-sentence)
(add-hook 'nanowrimo-mode 'hl-sentence-mode)
(set-face-attribute 'hl-sentence nil
                    :foreground "black"
                    :background "dark-olive-green")
;; (add-hook 'nanowrimo-mode 'variable-pitch-mode)
;; (set-face-attribute 'variable-pitch nil
;;                     :foreground "gray40")

(setq auto-mode-alist
  (append
   ;; File name (within directory) starts with a dot.
   '(
     ("\\.story\\'" . write-mode)
     )
   auto-mode-alist))
