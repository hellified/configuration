(use-package yasnippet-snippets)
(use-package yasnippet)
(setq yas-snippet-dirs
      '(
        ;;"~/.emacs.d/elpa/yasnippet-snippets-20220713.1234/snippets"
        "~/.emacs.d/snippets"                 ;; personal snippets
        ))
(yas-global-mode 1)

;;(define-key yas-minor-mode-map (kbd "<tab>") nil)
;;(define-key yas-minor-mode-map (kbd "TAB") nil)

;;(define-key yas-minor-mode-map (kbd "SPC") yas-maybe-expand)

;; ;; Bind `C-c y' to `yas-expand' ONLY.
;; (define-key yas-minor-mode-map (kbd "C-c y") #'yas-expand)
