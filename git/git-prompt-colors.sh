# This theme for gitprompt.sh is optimized for the "Solarized Dark" and "Solarized Light" color schemes
# based on "Solarized Extravagant", with user@host on the second line and some things removed.

function override_git_prompt_colors() {
    GIT_PROMPT_THEME_NAME='Hellified Custom'
    GIT_PROMPT_STAGED="${Yellow}● "
    GIT_PROMPT_UNTRACKED="${Red}… "
    GIT_PROMPT_STASHED="${BoldMagenta}⚑ "
    GIT_PROMPT_CLEAN="${Green}✔ "
    GIT_PROMPT_COMMAND_OK="${Green}✔ "
    GIT_PROMPT_COMMAND_FAIL="${Red}✘ "

    GIT_PROMPT_START_USER="_LAST_COMMAND_INDICATOR_ \e[1;30m ${PathShort}"
    GIT_PROMPT_START_ROOT="${GIT_PROMPT_START_USER}"
    GIT_PROMPT_END_USER="\n${Blue}\\u${White}@${BoldBlue}\\h ${BoldRed} ➤ ${ResetColor} "
    GIT_PROMPT_END_ROOT="\n${Blue}\\u${White}@${BoldBlue}\\h ${BoldRed} #️ ${ResetColor} "
    GIT_PROMPT_LEADING_SPACE=1
    GIT_PROMPT_PREFIX="${Blue}["
    GIT_PROMPT_SUFFIX="${Blue}]"
    GIT_PROMPT_SYMBOLS_NO_REMOTE_TRACKING="✭"
}

reload_git_prompt_colors 'Hellified Custom'
