#Git
a st="git status"
a pull="git pull"
a gdiff="rm -f ~/tmp/git_diff.diff;git diff --ignore-space-at-eol > ~/tmp/git_diff.diff; gedit ~/tmp/git_diff.diff"

# #########################################
# BEGIN: Git prompt
# #########################################

export GIT_PROMPT_ONLY_IN_REPO=1
# GIT_PROMPT_FETCH_REMOTE_STATUS=0   # avoid fetching remote status
# GIT_PROMPT_IGNORE_SUBMODULES=1 # avoid searching for changed files in submodules
# GIT_PROMPT_WITH_VIRTUAL_ENV=0 # avoid setting virtual environment infos for node/python/conda environments

# GIT_PROMPT_SHOW_UPSTREAM=1 # show upstream tracking branch
# GIT_PROMPT_SHOW_UNTRACKED_FILES=normal # can be no, normal or all; determines counting of untracked files

# GIT_PROMPT_SHOW_CHANGED_FILES_COUNT=0 # avoid printing the number of changed files

# as last entry source the gitprompt script
GIT_PROMPT_THEME=Custom # use custom theme specified in file GIT_PROMPT_THEME_FILE (default ~/.git-prompt-colors.sh)
GIT_PROMPT_THEME_FILE=~/.git-prompt-colors.sh
#export GIT_PROMPT_THEME=Solarized_UserHost

if [ -f /usr/lib/bash-git-prompt/gitprompt.sh ]
then
    source /usr/lib/bash-git-prompt/gitprompt.sh
elif [ -f ~/.bash-git-prompt/gitprompt.sh ]
then
     source ~/.bash-git-prompt/gitprompt.sh
fi

# #########################################
# END: Git prompt
# #########################################

if [ -f /usr/share/git/completion/git-completion.bash ]
then
    . /usr/share/git/completion/git-completion.bash
elif [ -f /usr/share/bash-completion/completions/git ]
then
    . /usr/share/bash-completion/completions/git
fi
