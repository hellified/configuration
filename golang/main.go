package main

import (
	"fmt"
	"os"
	"time"

	"github.com/rs/zerolog"
)

func main() {
	logger := zerolog.New(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339}).
		Level(zerolog.TraceLevel).With().Timestamp().Caller().Logger()

	fmt.Println("Hello, World.")
	logger.Info().Msg("logging is established")
}
