#!/usr/bin/env python3

import subprocess
import sys

packages = "ack adwaita-icon-theme alsa-lib alsa-plugins alsa-utils anki"
packages += " archlinux-keyring ario aspell aspell-en aspell-sv"
packages += " bash-completion blueman bluez bluez-libs bluez-utils bzip2"
packages += " calibre cups cups-filters cups-pdf curl "
packages += " deluge dialog docker docker-compose dosfstools"
packages += " emacs emelfm2 evince"
packages += " festival festival-english file findutils firefox flake8"
packages += " fontconfig foomatic-db foomatic-db-engine foomatic-db-nonfree"
packages += " fortune-mod freemind"
packages += " galculator gawk gedit gedit-code-assistance gedit-plugins geeqie"
packages += " gftp ghostscript gimp git gitg gnome-keyring gnupg grep"
packages += " gst-plugins-bad gst-plugins-base gst-plugins-base-libs"
packages += " gst-plugins-good gst-plugins-ugly gstreamer gtkspell3 guvcview"
packages += "  gzip"
packages += " hdparm hicolor-icon-theme hplip hspell htop hunspell hunspell-en"
packages += " hunspell-en_GB hunspell-en_US"
packages += " icedtea-web imagemagick"
packages += " java-environment-common java-openjfx jdk8-openjdk jre8-openjdk"
packages += " jre8-openjdk-headless"
packages += " kupfer"
packages += " less liferea lightdm lightdm-gtk-greeter links linux"
packages += " linux-api-headers linux-firmware linux-headers lm_sensors"
packages += " logrotate lshw lsof"
packages += " maven mcomix midori mlocate mpd"
packages += " nano nerd-fonts-source-code-pro network-manager-applet"
packages += " networkmanager nfs-utils nm-connection-editor"
packages += " notification-daemon"
packages += " openjdk8-doc openjpeg2 openssh orage"
packages += " p7zip pacman pacman-mirrorlist paprefs pavucontrol pgadmin4"
packages += " picard polkit polkit-gnome postgresql-libs pulseaudio"
packages += " pulseaudio-alsa pulseaudio-bluetooth pulseaudio-equalizer"
packages += " pulseaudio-zeroconf pygtk python python-pipenv python2-notify"
packages += " pkgfile"
packages += " rsync ruby"
packages += " sane sane-frontends sqlite sqlitebrowser subversion sudo swig"
packages += " thunar thunar-archive-plugin thunar-media-tags-plugin"
packages += " thunar-volman thunderbird tilda tk tlp tree trello ttf-dejavu"
packages += " ttf-freefont ttf-liberation"
packages += " unrar unzip usbutils util-linux"
packages += " v4l-utils vlc"
packages += " wget which wpa_supplicant"
packages += " x11-ssh-askpass xarchiver xfce4 xfce4-goodies xorg-server xsane"
packages += " xsane-gimp xscreensaver"
packages += " zip"

print("Checking packages:")
print()
for package in packages.split():
    result = subprocess.run(['aurman', '-Qi', f"{package}"],
                            stdin=subprocess.DEVNULL,
                            stdout=subprocess.DEVNULL)
    if result.returncode != 0:
        print()
        print(f"Installing: [{package}]")
        result = subprocess.run(['aurman', '-S', '--needed',
                                 '--noconfirm', f"{package}"],
                                stdin=subprocess.DEVNULL,
                                stdout=subprocess.DEVNULL)
        print()
        if result.returncode != 0:
            print(f"Install of {package} failed. Exiting.")
            exit(1)
    else:
        sys.stdout.write(str(result.returncode))
        sys.stdout.flush()
