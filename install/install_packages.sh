#!/usr/bin/bash 
trap "exit" INT

sudo pacman -Sy

packages=(ack adwaita-icon-theme alsa-lib alsa-plugins alsa-utils anki archlinux-keyring ario aspell aspell-en aspell-sv)
packages+=(bash-completion blueman bluez bluez-libs bluez-utils bzip2)
packages+=(calibre cups cups-filters cups-pdf curl )
packages+=(deluge dialog docker docker-compose dosfstools)
packages+=(emacs emelfm2 evince)
packages+=(festival festival-english file findutils firefox flake8 fontconfig foomatic-db foomatic-db-engine foomatic-db-nonfree fortune-mod freemind)
packages+=(galculator gawk gedit gedit-code-assistance gedit-plugins geeqie gftp ghostscript gimp git gitg gnome-keyring gnupg go-tools grep gst-plugins-bad gst-plugins-base gst-plugins-base-libs gst-plugins-good gst-plugins-ugly gstreamer gtkspell3 guvcview gzip)
packages+=(hdparm hicolor-icon-theme hplip hspell htop hunspell hunspell-en hunspell-en_GB hunspell-en_US)
packages+=(icedtea-web imagemagick)
packages+=(java-environment-common java-openjfx jdk8-openjdk jre8-openjdk jre8-openjdk-headless)
packages+=(kupfer)
packages+=(less liferea lightdm lightdm-gtk-greeter links linux linux-api-headers linux-firmware linux-headers lm_sensors logrotate lshw lsof)
packages+=(maven mcomix midori mlocate mpd)
packages+=(nano nerd-fonts-source-code-pro network-manager-applet networkmanager nfs-utils nm-connection-editor notification-daemon)
packages+=(openjdk8-doc openjpeg2 openssh orage)
packages+=(p7zip pacman pacman-mirrorlist paprefs pavucontrol pgadmin4 picard polkit polkit-gnome postgresql-libs pulseaudio pulseaudio-alsa pulseaudio-bluetooth pulseaudio-equalizer pulseaudio-zeroconf pygtk python python-pipenv python2-notify pkgfile)
packages+=(rsync ruby)
packages+=(sane sane-frontends sqlite sqlitebrowser subversion sudo swig)
packages+=(thunar thunar-archive-plugin thunar-media-tags-plugin thunar-volman thunderbird tilda tk tlp tree trello ttf-dejavu ttf-freefont ttf-liberation)
packages+=(unrar unzip usbutils util-linux)
packages+=(v4l-utils vlc)
packages+=(wget which wpa_supplicant)
packages+=(x11-ssh-askpass xarchiver xfce4 xfce4-goodies xorg-server xsane xsane-gimp xscreensaver)
packages+=(zip)

function contains() {
    [[ $1 =~ (^|[[:space:]])$2($|[[:space:]]) ]] && exit(0) || exit(1)
}

installed=(${pacman -Qqe})
echo "$installed"
exit

for package in "${packages[@]}";do
    
    if ! aurman -Qi $package; then
        echo "Installing: [$package]"
        aurman -S --needed --noconfirm "$package"
    fi
done

echo "Last shot, updating everything..."
aurman -Syu

echo "All done, nothing to see here."
