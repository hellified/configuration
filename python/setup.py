#!/usr/bin/env python
# -*- coding: utf-8 -*-
import io
from collections import OrderedDict

from setuptools import find_packages
from setuptools import setup

with io.open("README.md", "rt", encoding="utf8") as f:
    readme = f.read()

setup(
    name="feature-store",
    version='0.2',
    packages=find_packages(),
    project_urls=OrderedDict(
        (
            ("Documentation", ""),
            ("Code", "https://bitbucket.services.kambi.com/projects/DATSCI/repos/feature_store"),
        )
    ),
    author="Team Yakt",
    author_email="dev_team_yakt@kambi.com",
    maintainer="Data Science Team",
    maintainer_email="lawrence.green@kambi.com",
    description="Module for reading data from the kambi feature store.",
    long_description=readme,
    platforms="any",
    license='Proprietary',
    install_requires=[
        "pymongo ==3.7.2"
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "Environment :: Console",
        "License :: Other/Proprietary License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
    ]
)
