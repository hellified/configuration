#!/usr/bin/env bash
source "$HOME"/configuration/shell/bash/bash_common.sh
# ######################
# General Environment
# ######################
# rm -rf ~/.zshrc
# ln -s ~/configuration/shell/zsh/zshrc.sh        ~/.zshrc

rm -rf ~/.prompt.sh
ln -s ~/configuration/shell/zsh/prompt.sh       ~/.prompt.sh

rm -rf ~/.aliases.sh
ln -s ~/configuration/shell/aliases.sh          ~/.aliases.sh

rm -rf ~/.bashrc
ln -s ~/configuration/shell/bash/bashrc.sh      ~/.bashrc

rm -rf ~/.bash_profile
ln -s ~/configuration/shell/bash/bash_profile.sh      ~/.bash_profile

rm -rf ~/.bash_prompt.sh
ln -s ~/configuration/shell/bash/bash_prompt.sh ~/.bash_prompt.sh

if at_home ; then
    rm -rf ~/.makepkg.conf
    ln -s ~/configuration/arch/makepkg.conf         ~/.makepkg.conf
fi

# #################################################
# Desktop
# #################################################
rm -rf ~/.desktop.sh
ln -s ~/configuration/shell/desktop.sh ~/.desktop.sh

# #################################################
# Eye candy
# #################################################
mkdir -p ~/.config/conky
rm -rf ~/.config/conky/conky.conf
ln -s ~/configuration/desktop/apps/conky/conky."$(uname -n)".conf      ~/.config/conky/conky.conf


# #################################################
# development
# #################################################
rm -rf ~/devel/common
mkdir -p ~/devel/common/bash
ln -s ~/configuration/shell/bash/bash_common.sh ~/devel/common/bash/bash_common.sh

# ######################
# emacs
# ######################
rm -rf ~/.emacs
rm -rf ~/.emacs.d
ln -s ~/configuration/emacs/emacs.d             ~/.emacs.d
mkdir -p ~/.emacs.d/elpa

rm -rf ~/.emacs.sh
ln -s ~/configuration/shell/emacs.sh            ~/.emacs.sh

# ######################
# python
# ######################
rm -rf ~/.python.sh
ln -s ~/configuration/shell/python.sh           ~/.python.sh

mkdir -p ~/.config/pip
rm -rf ~/.config/pip/pip.conf
if at_home ; then
    ln -s ~/configuration/python/pip.home.conf             ~/.config/pip/pip.conf
fi

rm -rf ~/.pylintrc
ln -s ~/configuration/python/pylintrc          ~/.pylintrc

rm -rf ~/.direnvrc
ln -s ~/configuration/shell/direnv.rc       ~/.direnvrc

mkdir -p ~/.config/pypoetry
rm -rf  ~/.config/pypoetry/config.toml
ln -s ~/configuration/python/poetry/config.toml ~/.config/pypoetry/config.toml

# ####################
# ansible
# ####################
rm -rf ~/.ansible.cfg
ln -s ~/configuration/ansible/ansible.cfg          ~/.ansible.cfg

# ######################
# java
# ######################
rm -rf ~/.java.sh
ln -s ~/configuration/shell/java.sh             ~/.java.sh

# ######################
# docker
# ######################
rm -rf ~/.docker.sh
ln -s ~/configuration/shell/docker.sh           ~/.docker.sh

# kubernetes
rm -rf ~/.kubernetes.sh
ln -s ~/configuration/shell/kubernetes.sh       ~/.kubernetes.sh

# ######################
# podman
# ######################
rm -rf ~/.config/containers
# mkdir -p ~/.config/containers
# ln -s ~/configuration/podman/containers.conf    ~/.config/containers/containers.conf
# ln -s ~/configuration/podman/registries.conf    ~/.config/containers/registries.conf
# ln -s ~/configuration/podman/storage.conf       ~/.config/containers/storage.conf

rm -rf ~/.podman.sh
# ln -s ~/configuration/shell/podman.sh           ~/.podman.sh


# ######################
# ruby/rails
# ######################
rm -rf ~/.ruby.sh
ln -s ~/configuration/shell/ruby.sh             ~/.ruby.sh

rm -rf ~/.railsrc
ln -s ~/configuration/shell/railsrc             ~/.railsrc

# ######################
# go
# ######################
rm -rf ~/.golang.sh
ln -s ~/configuration/shell/golang.sh             ~/.golang.sh

# ######################
#  General dev
# ######################
rm -rf ~/.git.sh
ln -s ~/configuration/git/git.sh             ~/.git.sh

rm -rf ~/.git-prompt-colors.sh
ln -s ~/configuration/git/git-prompt-colors.sh ~/.git-prompt-colors.sh

rm -rf ~/.gitconfig
ln -s ~/configuration/git/gitconfig ~/.gitconfig

rm -rf ~/.cloud.sh
ln -s ~/configuration/shell/cloud.sh ~/.cloud.sh

rm -rf ~/.ssh.sh
ln -s ~/configuration/shell/ssh.sh        ~/.ssh.sh

rm -rf ~/.utilities.sh
ln -s ~/configuration/shell/utilities.sh        ~/.utilities.sh

# #########################
# bin directory
# #########################
rm -rf ~/bin
ln -s ~/configuration/bin       ~/bin

if at_home; then
    echo "At home, but skipping kubernetes and elixir setup for now."
    #setup_kubernetes
    #setup_elixir_emacs
fi

# ##########################
# Arch Linux
# ##########################
rm -rf ~/.arch-linux.sh
ln -s ~/configuration/shell/arch-linux.sh        ~/.arch-linux.sh

# #################################################
# NoGrade
# #################################################
rm -rf ~/.nograde.sh
ln -s ~/configuration/shell/nograde.sh ~/.nograde.sh



if ! at_home; then
    echo "Setting up paid work"
    source "$HOME"/configuration/setup_paid_work.sh
fi

echo "DONE"
