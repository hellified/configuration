#!/usr/bin/env bash
# ######################
# data engineering tools
# ######################
# rm -rf ~/.hadoop.sh
# ln -s ~/configuration/shell/hadoop.sh           ~/.hadoop.sh

# rm -rf ~/.spark.sh
# ln -s ~/configuration/shell/spark.sh             ~/.spark.sh

# rm -rf ~/.kafka.sh
# ln -s ~/configuration/shell/kafka.sh             ~/.kafka.sh

# rm -rf ~/.mongo.sh
# ln -s ~/configuration/shell/mongo.sh             ~/.mongo.sh

# rm -rf ~/.kambi_amazon.sh
# ln -s ~/configuration/shell/kambi_amazon.sh      ~/.kambi_amazon.sh

# rm -rf ~/.kambi_kubernetes.sh
# ln -s ~/configuration/shell/kambi_kubernetes.sh  ~/.kambi_kubernetes.sh

# ######################
# job related
# ######################
rm -rf ~/.paid_work.sh
ln -s ~/configuration/shell/paid_work.sh       ~/.paid_work.sh
# Hexagon
# Turner
# Kambi
# Nectarine
rm -rf ~/.nectarine.sh
ln -s ~/configuration/shell/paid_work/nectarine.sh           ~/.nectarine.sh

rm -rf ~/.google_cloud.sh
ln -s ~/configuration/shell/google_cloud.sh           ~/.google_cloud.sh
