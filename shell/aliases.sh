
alias a=alias

# Generally saving my ass
# a rm='rm -i'
# Fuck it, I'm a grown ass man!

# Directory listings
a ls='ls --color=auto -p -T $(/usr/bin/tput cols)'
a lt='ls -ltr'
a lh='ls -lh'
a la='ls -a -T $(/usr/bin/tput cols)'
a lta='ls -ltra'
a ll='ls -l'
a lla='ls -la'

a cleandir='find . -name "*~" -print -exec rm -f {} \;'

a j='jobs'

a cd..='cd ..'

a df="df -hT"
a du="du -hs *"
a duh="du -hs .[^.]*"

a pst="ps axjf"
a psg="ps aux | grep "

a highcpu="ps -eo %cpu,pid,cmd:999 --sort=-%cpu | head"
a highmem="ps -eo %mem,pid,cmd:999 --sort=-%mem | head"

a genv="env | grep "

a enw="emacs -nw"
a gh="history | grep "

a netstat=" netstat -tulpn"

# fast directory change
a ...='cd ../..'
a ....='cd ../../..'
a .....='cd ../../../..'
a ......='cd ../../../../..'
a .......='cd ../../../../../..'

function mcd () {
        mkdir -p "$*" && cd "$*"
}

a h='history'


if [ -x "$(command -v ack)" ]
then
    a acki='ack -i'
    a ackl='ack -l'
    a ackli='ack -li'

    function ackini() { ack $@ **/*.ini; }
    function ackj() { ack --java $@; }
    function ackjs() { ack --js $@; }
    function ackjson() { ack --json $@; }
    function ackp() { ack --python $@; }
    function ackq() { ack --sql $@; }
    function ackx() { ack --xml $@; }
fi

#Ansible
if [ -x "$(command -v ansible-playbook)" ]
then
    a ap="ansible-playbook"
fi

function yesterday() {
    find $@ -mtime -1 -ls | egrep -v "\.venv|\.git|cache"
}

function sum_col() {
    col_num=$1
    # cat - | awk -v col=$col_num '{print $col}' | grep -v "^$" |grep -v "[a-zA-Z]" | paste -sd+
    cat - | awk -v col=$col_num '{print $col}' | grep -v "^$" |grep -v "[a-zA-Z]" | paste -sd+ | bc | numfmt --grouping
}

function col_number(){
    col_name=$1
    cat - | awk -v name=$col_name '
    {
        for(i=1;i<=NF;i++) {
            if($i == name)
               printf("%s is column %d\n",name, i)
        }
        exit 0
    }
'
}

function kill_all_like() {
    pattern=$1
    ps -eaf | grep $pattern | grep -v grep | awk '{print $2}'| xargs kill -9
}

function clear_journal {
    sudo journalctl --rotate
    sudo journalctl --vacuum-time=2d
}

function keyboard_light {
    if (( $# != 1 )); then
        >&2 echo """Usage:
  keyboard_light <setting>
  Setting values:
    2 -> High
    1 -> Low
    0 -> Off
"""
    else
        brightnessctl --device='tpacpi::kbd_backlight' set $1
    fi
}

a sieve="sieve-connect -s monday.mxrouting.net -u isaac@bridgefire.net"

function year2front {
    perl-rename  's/^(.+).(\d\d\d\d)\.pdf/$2 - $1.pdf/' *.pdf
}

function numerical_month {
    declare -A months
    months["January"]="01"
    months["February"]="02"
    months["March"]="03"
    months["April"]="04"
    months["May"]="05"
    months["June"]="06"
    months["July"]="07"
    months["August"]="08"
    months["September"]="09"
    months["October"]="10"
    months["November"]="11"
    months["December"]="12"

    months["Jan"]="01"
    months["Feb"]="02"
    months["Mar"]="03"
    months["Apr"]="04"
    months["May"]="05"
    months["Jun"]="06"
    months["Jul"]="07"
    months["Aug"]="08"
    months["Sep"]="09"
    months["Oct"]="10"
    months["Nov"]="11"
    months["Dec"]="12"

    for month in ${!months[@]}
    do
        echo "[${month}] to [${months[${month}]}]"
        perl-rename -n -v "s/^(.+).(${month})\.pdf/$1 - ${months[${month}]}.pdf/" *.pdf
    done
}
