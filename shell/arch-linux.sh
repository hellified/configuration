# Arch Linux specific stuff

function mirror_list {
    curl -s "https://archlinux.org/mirrorlist/?country=SE&country=US&protocol=https&use_mirror_status=on" | sed -e 's/^#Server/Server/' -e '/^#/d' | rankmirrors -n 6 -
}
