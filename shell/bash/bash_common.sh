#!/usr/bin/env bash
# ###########################
# Output with coloring
# ###########################
NC='\033[0m' # No Color

BLACK='\033[0;30m'
function black() {
    echo >&2 -e "${BLACK}${1-}${NC}"
}

BLUE='\033[0;34m'
function blue() {
    echo >&2 -e "${BLUE}${1-}${NC}"
}

GREEN='\033[0;32m'
function green() {
    echo >&2 -e "${GREEN}${1-}${NC}"
}

PURPLE='\033[0;35m'
function purple() {
    echo >&2 -e "${PURPLE}${1-}${NC}"
}

RED="\033[0;31m"
function red() {
    echo >&2 -e "${RED}${1-}${NC}"
}

YELLOW='\033[1;33m'
function yellow() {
    echo >&2 -e "${YELLOW}${1-}${NC}"
}


# White='\033[1;37m'
# BrownOrange='\033[0;33m'
# Cyan='\033[0;36m'
# DarkGray='\033[1;30m'

function at_home() {
    if [[ $USER == 'isaac' ]]; then
        return 0
    else
        return 1
    fi
}

function run_direnv() {
    if [ -f ./.envrc ]; then
        direnv allow
        eval "$(direnv export bash)" 1> /dev/null 2>&1
    else
        red 'No .envrc found.'
    fi
}

function has_branch {
    branch_name=$1
    if git show-ref --verify --quiet "refs/heads/$branch_name"; then
        return 0
    else
        return 1
    fi
}


# Read and parse simple INI file
# Stolen from: https://sleeplessbeastie.eu/2019/11/11/how-to-parse-ini-configuration-file-using-bash/

# Get INI section
ReadINISections(){
  local filename="$1"
  gawk '{ if ($1 ~ /^\[/) section=tolower(gensub(/\[(.+)\]/,"\\1",1,$1)); configuration[section]=1 } END {for (key in configuration) { print key} }' "${filename}"
}

# Get/Set all INI sections
GetINISections(){
  local filename="$1"

  sections="$(ReadINISections "$filename")"
  for section in $sections; do
    array_name="configuration_${section}"
    declare -g -A "${array_name}"
  done
  eval "$(gawk -F= '{
                    if ($1 ~ /^\[/)
                      section=tolower(gensub(/\[(.+)\]/,"\\1",1,$1))
                    else if ($1 !~ /^$/ && $1 !~ /^;/) {
                      gsub(/^[ \t]+|[ \t]+$/, "", $1);
                      gsub(/[\[\]]/, "", $1);
                      gsub(/^[ \t]+|[ \t]+$/, "", $2);
                      if (configuration[section][$1] == "")
                        configuration[section][$1]=$2
                      else
                        configuration[section][$1]=configuration[section][$1]" "$2}
                    }
                    END {
                      for (section in configuration)
                        for (key in configuration[section]) {
                          section_name = section
                          gsub( "-", "_", section_name)
                          print "configuration_" section_name "[\""key"\"]=\""configuration[section][key]"\";"
                        }
                    }' "${filename}"
        )"


}

function countdown {
    echo "${1}"
    secs=$(($2))
    while [ $secs -gt 0 ]; do
        echo -ne "  --> $secs\033[0K\r"
        sleep 1
        : $((secs--))
    done
}

function countdown_notfiy {
    countdown "$1" "$2"
    notify-send "$1"
}

# Milliseconds to days
function ms2d {
    local ms=$1
    local d=$((ms / 86400000))
    printf '%d day(s)\n' $d
}

function repeat(){
  for ((i=0;i<$1;i++)); do
    eval ${*:2}
  done
}
