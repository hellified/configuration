
# Show all colors
function show_colors() {
    for((i=16; i<256; i++)); do
        printf "\e[48;5;${i}m%03d" $i;
        printf '\e[0m';
        [ ! $((($i - 15) % 6)) -eq 0 ] && printf ' ' || printf '\n'
    done
}

# <fg_bg> value of 38 or 48 respectively
# declare -A shell_colors
# shell_colors=( \
#     ['KMB-CZC6377SD8']=101 \
# )

# if [[ $shell_colors[$(hostname)] ]]; then
#     printf $('\e[38;5;${shell_colors[$(hostname)]}m ')
# fi

#printf '\e[38;5;196m Foreground color: red\n'
#printf '\e[48;5;0m Background color: black\n'
