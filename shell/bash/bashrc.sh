#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

source ~/devel/common/bash/bash_common.sh

HOSTNAME="$(hostname)"
HOSTNAME_SHORT="${HOSTNAME%%.*}"

export PATH=$PATH:/usr/sbin:/sbin:$HOME/bin

PS1='[\u@\h \W]\$ '

set bell-style visible

# history
export HISTCONTROL=ignoredups
HISTFILE=~/.bash_history
HISTSIZE=5000
SAVEHIST=5000

export BROWSER=brave

# Flush bash history after each command
export PROMPT_COMMAND='history -a'

# Probably need a development shell file.
export TOOLS_DIR=$HOME/tools

init_files=(aliases.sh)
init_files+=(cloud.sh)
init_files+=(nograde.sh)

if hash emacs >/dev/null 2>&1
then
    init_files+=(emacs.sh)
fi

if hash python3 >/dev/null 2>&1
then
    init_files+=(python.sh)
fi

if hash docker >/dev/null 2>&1
then
    init_files+=(docker.sh)
fi

if hash podman >/dev/null 2>&1
then
    init_files+=(podman.sh)
fi

if hash git >/dev/null 2>&1
then
    init_files+=(git.sh)
fi

if hash kubectl >/dev/null 2>&1
then
    init_files+=(kubernetes.sh)
fi

if [[ $(uname -a) == *"arch"* ]]
then
    init_files+=(arch-linux.sh)
fi

#If I'm at work my user id isn't usually 'isaac'
if ! at_home; then
init_files+=(paid_work.sh)
fi

for init_file in "${init_files[@]}"; do
    full_init_file="$HOME/.$init_file"
    if [ -e $full_init_file ]; then
        . "$full_init_file"
    else
        echo "Didn't find init file: $full_init_file"
    fi
done

export DE=xfce

export QT_STYLE_OVERRIDE=gtk

# Run on new shell
if hash fortune >/dev/null 2>&1
then
    echo ""
    fortune -a
    echo ""
fi

# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

# SSH able to pop remote screens
export XAUTHORITY=$HOME/.Xauthority

#get rid of accessability warnings
export NO_AT_BRIDGE=1

if [ -f "/usr/share/doc/pkgfile/command-not-found.bash" ]; then
    source /usr/share/doc/pkgfile/command-not-found.bash
fi

shopt -s globstar
shopt -s autocd
shopt -s extglob

source ~/.ssh.sh
source ~/.utilities.sh
source ~/.bash_prompt.sh

if hash direnv >/dev/null 2>&1
then
   eval "$(direnv hook bash)"
fi


#Set the title of the terminal to the last command
# :Issue w/this always setting to setGitPrompt
#trap 'printf "\033]0;%s\007" "${BASH_COMMAND//[^[:print:]]/}" >&2' DEBUG
if [ -f "/usr/share/nvm/init-nvm.sh" ]; then
    source /usr/share/nvm/init-nvm.sh
fi

export FREEPLANE_JAVA_HOME=/usr/lib/jvm/java-11-openjdk

export TZ=:/etc/localtime

# export UID=$(id -u)
# export GID=$(id -g)

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
# export SDKMAN_DIR="$HOME/.sdkman"
# [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

source  /opt/asdf-vm/asdf.sh
source "${XDG_CONFIG_HOME:-$HOME/.config}/asdf-direnv/bashrc"
export ASDF_DATA_DIR="$HOME/.asdf"

#export GBM_BACKEND=nvidia-drm
#export __GLX_VENDOR_LIBRARY_NAME=nvidia

if hash go >/dev/null 2>&1
then
    source "$HOME/.golang.sh"
fi

if hash javac >/dev/null 2>&1
then
    source "$HOME/.java.sh"
fi


# NUPHY75 keyboard
#To do this live, but in a way that resets on reboot:
#Enable FN keys
#echo -n 0 | sudo tee /sys/module/hid_apple/parameters/fnmode 

#Disable/Return to default
#echo -n 1 | sudo tee /sys/module/hid_apple/parameters/fnmode

# To do this so it stays on reboot. Optionally do step (1) above, then do
echo 'options hid_apple fnmode=0' | sudo tee -a /etc/modprobe.d/hid_apple.conf
