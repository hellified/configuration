# The next line updates PATH for the Google Cloud SDK.
if [ -f '/opt/google-cloud-cli/path.bash.inc' ]; then
    source '/opt/google-cloud-cli/path.bash.inc'
fi

# The next line enables shell command completion for gcloud.
if [ -f '/opt/google-cloud-cli/completion.bash.inc' ]; then
    source '/opt/google-cloud-cli/completion.bash.inc'
fi
