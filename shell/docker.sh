export DOCKER_DATA=$HOME/devel/docker_data

# ###########################
#  Images
# ###########################
a drmi="docker rmi"

function docker_rmi_all() {
    docker ps -a | awk 'NR>1{print $1 }' | xargs -I {} docker rm {}
    docker images | awk 'NR>1{ print $1":"$2}' | xargs -I {} docker rmi {}
    docker images | awk 'NR>1{ print $3}' | xargs -I {} docker rmi {}
}

alias dils="docker image ls | (tail -n +2 | sort)"

function dlabels() {
    docker inspect --format='{{json .ContainerConfig.Labels}}' $@ | jq
}

# ###########################
#  Processes
# ###########################
alias dps="docker ps --format \"{{.Names}}\t{{.ID}}\t{{.Ports}}\" | sort | column -t"
alias dpsa="docker ps -a --format \"{{.Names}}\t{{.ID}}\t{{.Status}}\t{{.Image}}\" | sort | column -t"

function dport {
    docker port $1 $2
}

function drma() {
    for container in $(docker ps --filter=status=exited --filter=status=created -q)
    do
        echo 'Removing $container'
        docker rm -v $container
    done
}

function dsh() {
    docker exec -i -t  $@ sh -c "stty cols $COLUMNS rows $LINES && sh";
}

function dshi() {
    docker run --rm --entrypoint '/bin/sh' -it $@
}

function dbash() {
    docker exec -i -t  $@ bash -c "stty cols $COLUMNS rows $LINES && bash";
}

function dbashi() {
    docker run --rm --entrypoint '/bin/bash' -it $@
}

function dbashir() {
    docker run --rm --entrypoint '/bin/bash' -it -u root $@
}

function drm() {
    docker rm -f $@;
}

function dsave(){
    docker save -o $@.tar $@
}

function dsavez(){
    docker save $@ | gzip > $@.tgz
}

function drestore(){
    docker load -i $@
}

# ###########################
#  Networking
# ###########################
alias dnls="docker network ls | (head -n 1 && tail -n +2 | sort)"
alias dnrm="docker network rm"
alias dnc="docker network create -d bridge"

function dnrma() {
    docker network ls --format "{{.Name}}" | xargs -I {} docker network rm {};
}

function dip() {
    docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $@;
}

# ###########################
#  Volumes#
###########################
alias dvls="docker volume ls | (tail -n +2 | sort)"
alias dvrm="docker volume rm $1"

function dvrma() {
    docker volume ls --format "{{.Name}}" | xargs -I {} docker volume rm {};
}

function drd(){
    for image in $(docker images --filter "dangling=true" -q)
    do
        docker rmi $image;
    done
}

# ###########################
#  Compose
# ###########################
a dcu="docker compose up -d"

function dcl() {
    if [ -f ./.env.devel ]; then
        echo "Using environment file: ./.env.devel"
        docker compose --env-file ./.env.devel logs -f $1
    else
        docker compose logs -f $1
    fi
}

a dcul="dcu && dcl"

a dcd="docker compose down"
a dcs="docker compose stop"
a dcr="dcd && dcu"
a dcrl="dcd && dcul"

a dcb="docker compose build"
a dcbu="docker compose up --build -d "
a dcbul="dcbu && dcl"

a dcbn="docker compose build --no-cache"
a dcbnu="dcbn && dcu"
a dcbnul="dcbn && dcul"

a dprune="drma && drd && dvrma && dnrma"

# TODO: Remove all past versions of images
