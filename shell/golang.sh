# ################
# golang.sh
# ################
# export GOBIN=$(go env GOPATH)/bin
# export PATH=$GOBIN:$PATH
export GOPROXY=https://proxy.golang.org
#export GO111MODULE=on
#export GOMODCACHE=${HOME}/.golang/pkg/mod
#export GOPROXY
asdf_update_golang_env() {
  local go_bin_path
  go_bin_path="$(asdf which go 2>/dev/null)"
  if [[ -n "${go_bin_path}" ]]; then
    abs_go_bin_path="$(readlink -f "${go_bin_path}")"

    go env -w GOROOT="$(dirname "$(dirname "${abs_go_bin_path}")")"
    export GOROOT="$(dirname "$(dirname "${abs_go_bin_path}")")"
    
    go env -w GOPATH="$(go env GOROOT)/packages"
    export GOPATH="$(go env GOROOT)/packages"
    
    go env -w GOBIN="$(go env GOROOT)/bin"
    export GOBIN="$(go env GOROOT)/bin"
    
    go env -w GOMODCACHE="$(go env GOROOT)/pkg/mod"
    export GOMODCACHE="$(go env GOROOT)/pkg/mod"
  fi
}
asdf_update_golang_env


declare -A GO_PROJECT_ROOTS=(
    [hellified]="gitlab.com/hellified"
    [nograde]="gitlab.com/nograde"
)

alias godoc="(cd $HOME; nohup godoc -http :6060 &)"

if [ -x "$(command -v ack)" ]
then
    function ackg() {
        ack --go \
            --ignore-dir=vendor \
            --ignore-dire=_docker \
            --ignore-dir=.go \
            --ignore-file=match:/.*_test\.go/ \
            --ignore-file=firstlinematch:/package\ mocks/ \
            --ignore-file=firstlinematch:/\/\/\ Code\ generated\ by\ MockGen\.\ DO\ NOT\ EDIT\./ \
            $@;
    }
    function ackgt() { ack --go --ignore-dir=vendor --ignore-dir=.go  $@; }
fi


function goinit {
    app_name=$1
    if [[ -v $2 ]]; then
        group=$2
    else
        group="nograde"
    fi
    project_root=${GO_PROJECT_ROOTS[$group]}

    mkdir $app_name
    (
        cd $app_name
        cp ~/configuration/golang/env.rc .envrc
        cp ~/configuration/golang/gitignore .gitignore
        cp ~/configuration/golang/Makefile Makefile
        cp ~/configuration/golang/air_runner.sh air_runner.sh
        cp ~/configuration/golang/test_monitor.sh test_monitor.sh

        mkdir cmd
        cp ~/configuration/golang/main.go main.go

        cp -R ~/configuration/golang/_docker .
        sed -i "s/#APP_NAME#/${app_name}/" _docker/compose.yaml

        mkdir models handlers docs assets

        sed -i "s/#BINARY#/${app_name}/" Makefile
        direnv allow
        source .envrc

        go mod init ${project_root}/$app_name
        go mod tidy

        make bootstrap

        git init
        echo "README for $app_name" > README.md
        git add .
        git commit -m "Initial commit"
    )
}
