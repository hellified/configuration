function source_if_exists {
    if [ -e $1 ]; then
        source $1
    else
        yellow "Not present to source: $1"
    fi
}
echo "3.1: $JAVA_HOME"
source_if_exists /etc/bdp/conf/env_files/bdp.hadoop.env
echo "3.2: $JAVA_HOME"
if [ -n "$HADOOP_HOME" ]; then
    export PATH=$PATH:$HADOOP_HOME/bin
fi

#source_if_exists /etc/bdp/conf/env_files/bdp.hdfs.env
source_if_exists /etc/bdp/conf/env_files/bdp.mongo.env
echo "3.3: $JAVA_HOME"
source_if_exists /etc/bdp/conf/env_files/kdp.kafka.hadoop.env
echo "3.4: $JAVA_HOME"

function hdfstree {
    hdfs dfs -ls -R $1 | awk '{print $8}' | sed -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/'
}

# lines in files
function hdfslines {
    hdfs dfs -ls -R $1 | grep txt | awk '{print $8}' | xargs hdfs dfs -cat | wc -l
}

# ######################
# HIVE/Beeline
# ######################
# On the gateway, as datavac: kinit -kt /etc/security/keytabs/datavac.keytab datavac-prod1@SERVICES.KAMBI.COM
#a bq="beeline -u 'jdbc:hive2://hdp-prod1-mgmt1.sth.kambi.com:10000/default;principal=hive/_HOST@REALM'"
a beeline='/etc/bdp/bin/kmb_beeline'

a hls="hdfs dfs -ls hdfs://$@"
a hrm="hdfs dfs -rm -r hdfs://$@"

function avro_count {
    for avro_file in $(hdfs dfs -ls -C ${1}/*/*.avro)
    do
        echo "$(hadoop jar $HOME/tools/avro-tools/avro-tools-1.10.0.jar count $avro_file): ${avro_file}"
    done
}

function parquet_count {
    for parquet_file in $(hdfs dfs -ls -C ${1}/*/*.parquet)
    do
        echo "$(hadoop jar $HOME/tools/parquet-tools/parquet-tools-1.11.1.jar rowcount $parquet_file): ${parquet_file}"
    done
}

# function count_s3_parquet {
# }
