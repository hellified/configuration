# ######################
# Kafka
# ######################
source ~/devel/common/bash/bash_common.sh
#GetINISections $APPLICATION_SECRETS_FILE
#export PATH="$PATH:/opt/kafka/bin"

kafkacat="docker run -it --network=host edenhill/kcat:1.7.1"
#kafkacat=kcat

export LOCAL_KAFKA_BROKER=localhost:9094
export LOCAL_ZOOKEEPER=localhost:2181

function prod_kafka {
    export kafka_env="prod"
}

function dev_kafka {
    export kafka_env="dev"
}

function stage_kafka {
    export kafka_env="stage"
}

function get_kafka_brokers {
    environment=$kafka_env
    if [[ "$environment" == "prod" ]]; then
        echo "$PRODUCTION_BOOTSTRAP_SERVERS"
    elif [[ "$environment" == "stage" ]]
    then
        echo "$STAGE_BOOTSTRAP_SERVERS"
    elif [[ "$environment" == "dev" ]]
    then
         echo "$DEV_BOOTSTRAP_SERVERS"
    else
        echo "UNKNOWN"
    fi
}

function get_zookeeper_quorum {
    environment=$kafka_env
    if [[ "$environment" == "prod" ]]
    then
        echo "$PRODUCTION_ZOOKEEPERS"
    elif [[ "$environment" == "stage" ]]
    then
        echo "$STAGE_ZOOKEPERS"
    elif [[ "$environment" == "dev" ]]
    then
        echo "$DEV_ZOOKEEPERS"
    else
        echo "UNKOWN"
    fi
}

function ktl {
    kafka-topics.sh --bootstrap-server $(get_kafka_brokers) --list
}

alias kltl="kafka-topics.sh --zookeeper $LOCAL_ZOOKEEPER --list"

function ktail {
    kafka-console-consumer.sh \
        --bootstrap-server $(get_kafka_brokers) \
        --property print.key=true \
        --property key.separator=" : " \
        --topic $@
}

function kltail {
    kafka-console-consumer.sh \
        --bootstrap-server $LOCAL_KAFKA_BROKER \
        --property print.key=true \
        --property key.separator=" : " \
        --topic $@
}

function kshow {
    $kafkacat \
         -b $(get_kafka_brokers) \
         -C \
         -t $@

}

function klast {
    exec_klast $@
}

function tklast {
    exec_klast test $@
}

function lklast {
    exec_klast local $@
}

function exec_klast {

    $kafkacat \
         -b $(get_kafka_brokers) \
         -C \
         -o -$1 \
         -c $1 \
         -e \
         -q \
         -t $2
}

function kgrep {
    $kafkacat \
         -b $(get_kafka_brokers) \
         -o beginning \
         -e \
         -t $1 \
        | grep $2
}

function kcat {
    $kafkacat
         -b $(get_kafka_brokers) \
         -o beginning \
         -e \
         -t $1
}

# function ksql() {
#     docker run \
#            --rm \
#            -it \
#            --network="localstack_dev-data-network" \
#            confluentinc/cp-ksql-cli \
#            http://kafka-sql:8088
# }

# alias mmr="kafka-consumer-groups.sh --bootstrap-server cid-v2.dev.kambi.com:9092 --group dev-docker-mirror --reset-offsets --to-earliest --all-topics --execute"

# View the configuration for a kafka topic (retention, etc...)
#LISTENER_KERBEROS_BROKERS_STRING \
function ktc {
    kafka-configs.sh --bootstrap-server $(get_kafka_brokers) \
                     --all --describe \
                     --entity-type topics \
                     --entity-name $@
}

# Topic properties
function ktp {
    kafka-topics.sh --bootstrap-server $(get_kafka_brokers) \
                    --describe \
                    --topic $@
}

# topics list for a group
function kgtl {
    kafka-consumer-groups.sh  \
        --bootstrap-server $(get_kafka_brokers) \
        --timeout 9999 \
        --describe \
        --group $@ | sort
    #date
}

# members for a group
function kgm {
    kafka-consumer-groups.sh  \
        --bootstrap-server $(get_kafka_brokers) \
        --timeout 9999 \
        --describe \
        --group $@ \
        --members

    date
}

# list groups
function kgls {
    kafka-consumer-groups.sh \
        --bootstrap-server  $(get_kafka_brokers) \
        --timeout 9999 \
        --list | sort
}

function kgroups_backlog {
    for group in $(kgls)
    do
        echo "$group"
        # echo "$group  $(kgtl $group | sum_col 6)"
    done
}

# remove group
function kgrm {
    kafka-consumer-groups.sh \
        --bootstrap-server  $(get_kafka_brokers) \
        --delete \
        --group $@
    date
}

function kwrite() {
    kafka-console-producer.sh \
        --broker-list $(get_kafka_brokers) \
        --property "parse.key=true" \
        --property "key.separator=#" \
        --topic $@
}

# # topic offset for a group (not executed??)
# function kgto() {
#     kafka-consumer-groups.sh \
#         --bootstrap-server $(get_kafka_brokers) \
#         --command-config $(get_consumer_properties) \
#         --group $1 \
#         --topic $2 \
#         --reset-offsets \
#         --to-earliest
# }

# # reset topic offest for a group
# function kgtr() {
#     kafka-consumer-groups.sh \
#         --bootstrap-server $(get_kafka_brokers) \
#         --group $1 \
#         --topic $2 \
#         --reset-offsets \
#         --to-earliest \
#         --timeout 9999 \
#         --execute | sort
# }

# # reset offsets on all topics for a group
# function kgra() {
#     kafka-consumer-groups.sh \
#         --bootstrap-server $LISTENER_KERBEROS_BROKERS_STRING \
#         --group $@ \
#         --reset-offsets \
#         --to-earliest \
#         --execute \
#         --all-topics
# }

function klag() {
    for group in $(kgls)
    do
        #echo "# #################[ $group ]#############################"
        #kgtl $group
        echo "$group $(kgtl $group | sum_col 6)"
        #echo
        #echo "# ##############################################"
    done

}
