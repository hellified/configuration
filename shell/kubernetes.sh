export PATH=~/.kubectx:${PATH}

alias k=kubectl
# kubetail is awesome. Install it.

source <(kubectl completion bash)

# function minikube_init {
#     export AWS_ACCESS_KEY_ID=test
#     export AWS_SECRET_ACCESS_KEY=test
# }

function minikube_install {
    yay -S libvirt iptables-nft bridge-utils openbsd-netcat minikube helm chart-testing
}

function minikube_stop {
    minikube stop

    echo "==> Minikube status:"
    minikube status
}

function minikube_start {
    minikube start --kubernetes-version 1.21.6 --memory 4g --cpus 2 --driver=kvm2 #docker #kvm2

    echo "==> List of minikube domains (there should be a minikube domain)"
    virsh list --all

    echo "==> List of minikube networks (there should be a default and a mk-minikube network)"
    virsh net-list

    echo "==> Minikube status:"
    minikube status

    # echo ""
    # echo "Switching docker to use minikube"
    # eval $(minikube -p minikube docker-env)
    minikube addons enable dashboard
    minikube addons enable metrics-server

    minikube dashboard --url &
}

function klogs {
    kubectl logs -f $1 | tee $HOME/tmp/klog.log
}

function killpods {
    kubectl delete pod,service $(kubectl get pods -n default | grep $1 | awk '{print $1}')
}

function tail_pod {
    namespace=$1
    pod_pattern=$2
    pod_name=$(kubectl get pods -n $namespace | grep $pod_pattern | awk '{print $1}')

    echo "Tailing pod [${pod_name}] in namespace [${namespace}]"
    kubectl -n ${namespace} logs -f ${pod_name}
}

function pod_bash {
    namespace=$1
    pod_pattern=$2
    container=$3
    pod_name=$(kubectl get pods -n $namespace | grep $pod_pattern | awk '{print $1}')
    if [ ${container+x} ]; then
        echo "Hopping container [${container}] in pod [${pod_name}] in namespace [${namespace}]"
        kubectl exec -it ${pod_name} -n ${namespace} -c ${container} -- bash
    else
        echo "Hopping into pod [${pod_name}] in namespace [${namespace}]"
        kubectl exec -it ${pod_name} -n ${namespace} -- bash
    fi
}

function pod_sh {
    namespace=$1
    pod_pattern=$2
    pod_name=$(kubectl get pods -n $namespace | grep $pod_pattern | awk '{print $1}')

    echo "Hopping into pod [${pod_name}] in namespace [${namespace}]"
    kubectl exec -it ${pod_name} -n ${namespace} -- sh
}

if hash minikube >/dev/null 2>&1
then
   eval $(minikube docker-env --shell=bash)
fi
