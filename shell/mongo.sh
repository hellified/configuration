# Docker
export DOCKER_MONGO="localhost:27017"

function dockll {
    docker exec -it mongodb mongo -u kambidp -p sososecret --authenticationDatabase internal internal
}

function dockfs {
    docker exec -it mongodb mongo -u kambidp -p sososecret --authenticationDatabase features features
}

# ################################################################
export MONGO_ROUTER_LOADBALANCER=$PROD_MONGO
export MONGO_PORT=50001

export MONGO_DB_INTERNAL_USERID=bdp_internal_r
export MONGO_DB_INTERNAL_PASSWORD_NAME=database_passwords.mongo.internal.prod_ro_user

export MONGO_DB_FEATURES_USERID=bdp_features_r
export MONGO_DB_FEATURES_PASSWORD_NAME=database_passwords.mongo.features.prod_ro_user

export MONGO_DB_PREDICTION_USERID=bdp_prediction_r
export MONGO_DB_PREDICTION_PASSWORD_NAME=database_passwords.mongo.prediction.prod_ro_user

export PROD_MONGO="mongoquery.stk.kambi.com:50001"

function internal_ro_pw {
    echo $(awk -F "=" '/${MONGO_DB_INTERNAL_PASSWORD_NAME}/ {print $2}' ~/.secrets/creds.ini)
}

function features_ro_pw {
    echo $(awk -F "=" '/${MONGO_DB_FEATURES_PASSWORD_NAME}/ {print $2}' ~/.secrets/creds.ini)
}

function prediction_ro_pw {
    echo $(awk -F "=" '/${MONGO_DB_PREDICTION_PASSWORD_NAME}/ {print $2}' ~/.secrets/creds.ini)
}


# ############
export MONGO_TMP_DIR=$HOME/tmp/mongo_tmp
mkdir -p $MONGO_TMP_DIR

# Production
function prodll {
    cd ~/.docker_sessions/mongo/prodll

    docker run \
           -it \
           --rm \
           -v $(pwd)/:/home/mongodb \
           mongo:4.0.16 \
           mongo "mongodb://${MONGO_DB_INTERNAL_USERID}:$(internal_ro_pw)@$PROD_MONGO/internal"
    cd -
}

function prodll_script {
    echo "mongodb://${MONGO_DB_INTERNAL_USERID}:$(internal_ro_pw)@$PROD_MONGO/internal"
    docker run \
           --rm \
           -v $(pwd)/:/home/mongodb mongo:4.0.10 \
           mongo \
           "mongodb://${MONGO_DB_INTERNAL_USERID}:$(internal_ro_pw)@$PROD_MONGO/internal" $1 | tee $1.output
}

function prodfs {
    cd ~/.docker_sessions/mongo/prodfs
    docker run \
           -it \
           --rm \
           -v $(pwd)/:/home/mongodb \
           mongo:4.0.16 \
           mongo "mongodb://${MONGO_DB_FEATURES_USERID}:$(features_ro_pw)@$PROD_MONGO/features"
    cd -
}

function prodfs_script {
    container_script="/tmp/$(basename $1)"
    docker run \
       --rm \
       -v ${MONGO_TMP_DIR}:/tmp \
       mongo:4.0.16 \
       mongo "mongodb://${MONGO_DB_FEATURES_USERID}:$(features_ro_pw)@$PROD_MONGO/features"  $container_script | tee $1.output
}

function prodpred {
    cd ~/.docker_sessions/mongo/prodpred
    docker run \
           -it \
           --rm \
           -v $(pwd)/:/home/mongodb \
           mongo:4.0.16 \
           mongo "mongodb://${MONGO_DB_PREDICTION_USERID}:$(prediction_ro_pw)@$PROD_MONGO/prediction"
    cd -
}

function wrap_query {
    echo "Query to wrap: $1"
    echo "printjson(${1}.toArray())"
}

function ffs_ids {
    tmp_sql_file=$(mktemp -t mongo_sql.XXXXXXXX -p $MONGO_TMP_DIR)
    chmod 777 $tmp_sql_file
    #trap "rm -f ${full_tmp_sql_file}" EXIT
    query="db.metadata.find({},{\"_id\": 0, \"featureId\": 1, \"metadata.name\": 1}).sort({\"featureId\": 1})"
    echo "Query: $query"

    wrapped_query=$(wrap_query $query)
    echo "Wrapped query: $wrapped_query"
    echo $(wrap_query ${query}) > $tmp_sql_file
    cat $tmp_sql_file
    prodfs_script $tmp_sql_file
}

# Mongo sink
a srkms="ssh kdpmongosink1-release1.dev.kambi.com"
# http://kdpmongosink1-release1.dev.kambi.com:8086/connectors
# http://kdpmongosink1-release1.dev.kambi.com:8086/connectors/mongo-sink-v1/status
# http://kdpmongosink1-release1.dev.kambi.com:8086/connectors/mongo-sink-v2/status
# http://kdpmongosink1-release1.dev.kambi.com:8086/connectors/mongo-sink-prediction/status
# http://kdpmongosink1-release1.dev.kambi.com:8086/connectors/mongo-sink-feature/status

# http://kdpmongosink2-release1.dev.kambi.com:8086/connectors
# http://kdpmongosink2-release1.dev.kambi.com:8086/connectors/mongo-sink-v1/status
# http://kdpmongosink2-release1.dev.kambi.com:8086/connectors/mongo-sink-v2/status
# http://kdpmongosink2-release1.dev.kambi.com:8086/connectors/mongo-sink-prediction/status
# http://kdpmongosink2-release1.dev.kambi.com:8086/connectors/mongo-sink-feature/status

# Mongo
a srmongo="ssh kdpmongo1-release1.dev.kambi.com"
# mongo -u ${MONGO_DB_INTERNAL_USERID}w -p <password> -authenticationDatabase internal mongodb://kdpmongorouter1-release1.dev.kambi.com:50001

# sudo -u kdp_kafka_mongo_sink_prod /bin/bash
a skmw1="ssh kdp-kafka-mongo-sink1.stk.kambi.com "
a skmw2="ssh kdp-kafka-mongo-sink2.stk.kambi.com "

# #############################
# useful
# #############################

# Dump
function dump_mongo {
    # 1 environment
    # 2 database
    # 3 collection
    if [ -z ${3+x} ]; then
        data_file=${1}.${2}.dump
    else
        data_file=${1}.${2}.${3}dump
    fi

    docker run --rm -v $HOME/devel/data/mongo:/workdir/ -w /workdir/ mongo:4.0 mongodump --archive="/workdir/${data_file}"  --readPreference secondaryPreferred --uri "mongodb://${MONGO_DB_INTERNAL_USERID}:qrrwz7b8Pa3YQyZ3@$PROD_MONGO/internal"
}

function dump_prod_internal {
    if [ -z ${1+x} ]; then
        data_file=${1}.dump.dat
    else
        data_file=
    fi

    docker run --rm -v $HOME/devel/data/mongo:/workdir/ -w /workdir/ mongo:4.0 mongodump --archive="/workdir/prod.internal.testing.data.betoffer.changes.BetOfferOutcomePriceUpdated.dump"  --uri "mongodb://${MONGO_DB_INTERNAL_USERID}:$(internal_ro_pw)@$PROD_MONGO/internal" --collection "testing.data.betoffer.changes.BetOfferOutcomePriceUpdated"
}





#docker run --rm -v $(pwd):/workdir/ -w /workdir/ mongo:4.0 mongodump --archive="/workdir/dump/mongodump-features-db" --uri "mongodb://${MONGO_DB_FEATURES_USERID}:3SF529Redk3eTbx4@bdp-mongorouter-dev1.sth.kambi.com:50001/features"

# Restore (Two flavors)
# docker exec -i mongodb sh -c 'mongorestore -u kambidp -p sososecret --authenticationDatabase features --archive' < ./dump/mongodump-features-db

# docker exec -i mongodb sh -c 'mongorestore -u kambidp -p sososecret --authenticationDatabase features --archive=/dumpdir/dumps/mongodump-features-db'
