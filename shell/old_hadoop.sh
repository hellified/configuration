TOOLS_DIR=$HOME/tools

# HADOOP_VERSION=3.1.1
# SPARK_VERSION=2.3.2
# HIVE_VERSION=3.1.0

# SCALA_VERSION=2.11

# SOURCE_BOX=hdp-jupyter02-prod.bdp.kambi.com

# # ########################
# # Hadoop
# # ########################
# export HADOOP_HOME=$TOOLS_DIR/hadoop
# export HADOOP_OPTS="$HADOOP_OPTS -Djava.library.path=$HADOOP_HOME/lib/native"
# export HADOOP_COMMON_LIB_NATIVE_DIR=$HADOOP_HOME/lib/native
# export HADOOP_MAPRED_HOME=$HADOOP_HOME
# export HADOOP_COMMON_HOME=$HADOOP_HOME
# export HADOOP_HDFS_HOME=$HADOOP_HOME

# export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop/

# export LD_LIBRARY_PATH=$HADOOP_COMMON_LIB_NATIVE_DIR
# export YARN_HOME=$HADOOP_HOME

# # ########################
# # Spark
# # ########################
# export SPARK_HOME=$TOOLS_DIR/spark
# export SPARK_LOCAL_IP="127.0.0.1"
# export SPARK_JARS=$SPARK_HOME/jars

# # ########################
# # Hive
# # ########################
# export HIVE_HOME=$TOOLS_DIR/hive


# export PATH=$PATH:$HIVE_HOME/bin:$SPARK_HOME/bin:$HADOOP_HOME/bin

# # ########################
# # Setup
# # ########################
# install_hadoop() {
#     unlink $HADOOP_HOME
#     echo "Pulling down hadoop..."
#     curl -Lko /tmp/hadoop-$HADOOP_VERSION.tar.gz \
#          https://www.apache.org/dist/hadoop/common/hadoop-$HADOOP_VERSION/hadoop-$HADOOP_VERSION.tar.gz
#     mkdir -p $HADOOP_HOME-$HADOOP_VERSION
#     tar -xvf /tmp/hadoop-$HADOOP_VERSION.tar.gz -C $HADOOP_HOME-$HADOOP_VERSION --strip-components=1 \
#         && rm /tmp/hadoop-$HADOOP_VERSION.tar.gz

#     ln -s $HADOOP_HOME-$HADOOP_VERSION $HADOOP_HOME
# }

# install_hive() {
#     unlink $HIVE_HOME
#     echo "Pulling down hive ..."
#     curl -Lko /tmp/hive-$HIVE_VERSION.tar.gz \
#          https://archive.apache.org/dist/hive/hive-$HIVE_VERSION/apache-hive-$HIVE_VERSION-bin.tar.gz
#     mkdir -p $HIVE_HOME-$HIVE_VERSION
#     tar -xvf /tmp/hive-$HIVE_VERSION.tar.gz -C $HIVE_HOME-$HIVE_VERSION --strip-components=1 \
#         && rm /tmp/hive-$HIVE_VERSION.tar.gz
#     ln -s $HIVE_HOME-$HIVE_VERSION $HIVE_HOME
# }

# install_spark() {
#     unlink $SPARK_HOME
#     echo "Pulling down spark..."
#     curl -Lko /tmp/spark-$SPARK_VERSION-bin-without-hadoop.tgz \
#          https://www.apache.org/dist/spark/spark-$SPARK_VERSION/spark-$SPARK_VERSION-bin-without-hadoop.tgz
#     mkdir -p $SPARK_HOME-$SPARK_VERSION
#     tar -xvf /tmp/spark-$SPARK_VERSION-bin-without-hadoop.tgz -C $SPARK_HOME-$SPARK_VERSION --strip-components=1 \
#         && rm /tmp/spark-$SPARK_VERSION-bin-without-hadoop.tgz

#     ln -s $SPARK_HOME-$SPARK_VERSION $SPARK_HOME

#     #
#     cp $SPARK_HOME/conf/spark-defaults.conf.template $SPARK_HOME/conf/spark-defaults.conf
#     echo 'spark.io.compression.codec org.apache.spark.io.SnappyCompressionCodec' >> $SPARK_HOME/conf/spark-defaults.conf
#     echo "spark.driver.memory 8g" >> $SPARK_HOME/conf/spark-defaults.conf
#     echo "PYSPARK_PYTHON=python3" >> $SPARK_HOME/conf/spark-env.sh
#     cp $SPARK_HOME/conf/log4j.properties.template $SPARK_HOME/conf/log4j.properties
#     sed -i 's/INFO/ERROR/g' $SPARK_HOME/conf/log4j.properties



# }

# copy_hadoop_conf() {
#     scp -r $SOURCE_BOX:/etc/hadoop/conf/*  $HADOOP_CONF_DIR
#     sudo mkdir -p /usr/hdp
#     sudo chmod -R 777 /usr/hdp
#     scp -r $SOURCE_BOX:/usr/hdp/* /usr/hdp
# }

# install_big_data_platform() {
#     install_hadoop && install_spark && install_hive
# }

hdfstree() {
    hdfs dfs -ls -R $1 | awk '{print $8}' | sed -e 's/[^-][^\/]*\//--/g' -e 's/^/ /' -e 's/-/|/'
}

# lines in files
hdfslines() {
    hdfs dfs -ls -R $1 | grep txt | awk '{print $8}' | xargs hdfs dfs -cat | wc -l
}

# # ########################
# # Spark
# # ########################
# export SPARK_DIST_CLASSPATH=$($HADOOP_HOME/bin/hadoop classpath)

# ######################
# HIVE/Beeline
# ######################
# On the gateway, as datavac: kinit -kt /etc/security/keytabs/datavac.keytab datavac-prod1@SERVICES.KAMBI.COM
a bq="beeline -u 'jdbc:hive2://hdp-prod1-mgmt1.sth.kambi.com:10000/default;principal=hive/_HOST@REALM'"


a hls="hdfs dfs -ls hdfs://$@"
a hrm="hdfs dfs -rm -r hdfs://$@"
