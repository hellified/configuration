export AKKA_HOME="$HOME/bin/scala/akka"
export PATH=$HOME/bin/activator:$PATH

alias mt="rm -rf c:/inpursuit/mobileserver/logs/mobile-server.2016-0*;sbt clean test -Dconfig.resource=isaac.pg.conf"

alias kt="rm -rf c:/inpursuit/mobileserver/logs/mobile-server.2016-0*;sbt \"test-only *KeycloakClientSpec\" -Dconfig.resource=isaac.pg.conf"

alias sclean="sbt clean"
alias scov="sclean coverage test -Dconfig.resource=isaac.pg.conf"


# function 1t() {
#     rm -rf c:/inpursuit/mobileserver/logs/mobile-server.2016-0*;
#     sbt "test-only *$@" -Dconfig.resource=isaac.pg.conf
# }
