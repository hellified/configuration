# ###########################
# Kambi specific settings
# ###########################
function on_local {
    echo "Hostname: $(hostname)"
    if [[ $(hostname) == KMB* ]]; then
        return 0
    else
        return 1
    fi
}

export APPLICATION_SECRETS_FILE=$HOME/.secrets/creds.ini

source ~/.kafka.sh
source ~/.mongo.sh
source ~/.spark.sh

# ######################
# DBVisualizer
# ######################
export PATH=$PATH:$TOOLS_DIR/DbVisualizer:$TOOLS_DIR/bin

# dev_team_yakt
# AKCp5Zjpigg25JzCkAQeZUTrNH25fhc6mvUYDqJ2gfNDzYJgVDZ7wvQre8QojjeQN4jYHRW3G

a tupload="twine upload --repository-url http://artifactory.sth.dev.kambi.com/artifactory/api/pypi/pypi-local dist/*"

# ######################
# Scala
# ######################
export SCALA_HOME=/usr/share/scala-2.11


# ######################
# STH
# ######################
#a sgw1="ssh hdp-prod1-gw1.sth.kambi.com"

a smm1="ssh hdp-prod1-mirrormaker1.sth.kambi.com"
a smm2="ssh hdp-prod1-mirrormaker2.sth.kambi.com"

a skf7="ssh hdp-prod1-kafka7.sth.kambi.com"
a skf9="ssh hdp-prod1-kafka9.sth.kambi.com"
a skf10="ssh hdp-prod1-kafka10.sth.kambi.com"

#a skw1="ssh hdp-prod1-kafkawriter1.sth.kambi.com"
#a skw2="ssh hdp-prod1-kafkawriter2.sth.kambi.com"

a smas4="ssh hdp-prod1-master4.sth.kambi.com"
a smas5="ssh hdp-prod1-master5.sth.kambi.com"
a smas6="ssh hdp-prod1-master6.sth.kambi.com"

a sair="ssh hdp-prod1-airflow1.sth.kambi.com"

a sdsc="ssh vc84nx870ta.inv.kambi.com"
a sj3="ssh hdp-prod1-jupyter3.sth.kambi.com"

# ######################
# STK
# ######################
# datadev_workflow_user
# ######################

# sudo -u kdp_hdfs_sink_production /bin/bash
a skhw1="ssh hdp-kafkawriter1-prod.bdp.kambi.com"
a skhw2="ssh hdp-kafkawriter2-prod.bdp.kambi.com"

a sgw1="ssh hdp-gw01-prod.bdp.kambi.com"

a sj2="ssh hdp-jupyter02-prod.bdp.kambi.com"

a stest="ssh litu1-test24.dev.kambi.com"

# ###############################
# Build box for python stuff
# ###############################
a sjp="ssh jenkins-pythondev1.dev.kambi.com"

# ###############################
# CID environment (DataScience)
# ###############################
a scid="ssh kdp-app1-cid.dev.kambi.com"
# zookeeper1-cid.dev.kambi.com:2181

# ###############################
# Release environment
# ###############################
# Mongo: kdpmongorouter1-release1.dev.kambi.com:50001
# Mongo box: kdpmongo1-release1.dev.kambi.com
#
# ZOOKEPERS_QUORUM_STRING="kdpzookeeper1-release1.dev.kambi.com:2181,
#                          kdpzookeeper2-release1.dev.kambi.com:2181,
#                          kdpzookeeper3-release1.dev.kambi.com:2181"
#
# LISTENER_KERBEROS_BROKERS_STRING="kdpkafka1-release1.dev.kambi.com:9092,
#                                   kdpkafka2-release1.dev.kambi.com:9092,
#                                   kdpkafka3-release1.dev.kambi.com:9092"
#
# Kafka mongo sink: kdpmongosink1-release1.dev.kambi.com
#
# Mirror maker: kdpmirrormaker1-release1.dev.kambi.com
#               kdpmirrormaker2-release1.dev.kambi.com

# Application
a srls="ssh kdpapp1-release1.dev.kambi.com"
# sudo su - [featuregen|featurecoll]

# Kafka
a srkafka="ssh kdpkafka1-release1.dev.kambi.com"

# ######################
# test environment
# ######################
#  ssh test24-v1.dev.kambi.com
#  ssh test24-v3.dev.kambi.com
#  ssh test24-v2.dev.kambi.com

a sjen1="ssh hdp-jenkins01-prod.bdp.kambi.com"
a sjen2="ssh hdp-jenkins02-prod.bdp.kambi.com"
# sudo -u datadev_workflow_user  -s -- /bin/bash

a stest42="ssh kdp-kafka2-test42.dev.kambi.com"

# ######################
# Cloud
# ######################
a sjsr="ssh yakt-prod-jobstatusreporter1.datatooling.prod-eu-north-1.kambi.com"

# ######################
# Artifactory
# ######################
export ARTIFACTORY_HOST=artifactory.services.kambi.com
export ARTIFACTORY_USERNAME="dev_team_yakt"
export ARTIFACTORY_PASSWORD="AKCp5Zjpigg25JzCkAQeZUTrNH25fhc6mvUYDqJ2gfNDzYJgVDZ7wvQre8QojjeQN4jYHRW3G"

export TWINE_USERNAME=$ARTIFACTORY_USERNAME
export TWINE_PASSWORD=$ARTIFACTORY_PASSWORD

a sjobstatus="ssh yakt-dev-jobstatusreporter1.dataplatform-apps.test-eu-north-1.kambi.com"

# #######################
# NVM
# #######################
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

# #######################
# Ansible
# #######################
a ansible="ansible -i /home/isahod/devel/shed/ansible/inventory.ini"
a ap="ansible-playbook -i /home/isahod/devel/shed/ansible/inventory.ini"


# #######################
# Office proxy
# #######################
function set_proxies {
    proxy=$1
    export http_proxy=$proxy
    export https_proxy=$proxy
    export ftp_proxy=$proxy
    export HTTPS_PROXY=$proxy
    export HTTP_PROXY=$proxy
    export FTP_PROXY=$proxy

    if on_local; then
        run_docker_proxy_template
        reload_docker $proxy
    fi

}

function run_docker_proxy_template {
    proxy=$1
    envsubst < $HOME/configuration/docker/kambi/http-proxy.template.conf > /tmp/http-proxy.conf
    sudo rm -f /etc/systemd/system/docker.service.d/*-proxy.conf
    sudo cp /tmp/http-proxy.conf /etc/systemd/system/docker.service.d/
}

function reload_docker {
    echo "Reloading docker ..."
    sudo systemctl daemon-reload
    sudo systemctl restart docker
}

# function proxy_buho {
#     set_proxies "http://proxy.buho.kambi.com:3128"
# }

# function proxy_lndo {
#     set_proxies "http://proxy.lndo.kambi.com:3128"
# }

# function proxy_mano {
#     set_proxies "http://proxy.mano.kambi.com:3128"
# }

# function proxy_stno {
#     set_proxies "http://proxy.stno.kambi.com:3128"
# }

# function proxy_phlo {
#     set_proxies "http://proxy.phlo.kambi.com:3128"
# }

function proxy_office {
    set_proxies "http://officeproxy.services.kambi.com:3128"
}


function proxy_local {
    set_proxies "http://127.0.0.1:3128"
}


function nproxy {
    unset http_proxy
    unset https_proxy
    unset ftp_proxy
    unset HTTPS_PROXY
    unset HTTP_PROXY
    unset FTP_PROXY

    if on_local; then
        sudo rm -f /etc/systemd/system/docker.service.d/*-proxy.conf
        reload_docker $proxy
    fi

}

# printf "\x1b[41m\x1b[2JPRODUCTION SYSTEM [%n]\n\n"u


# Node version manager
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# ###############################
# No VPNs configured? Do this as root
# ###############################
# puppet agent -t
# getvpn.sh -f
# #########################################
function start_vpn {
    nmcli connection up $1\ \($USER\) --ask
}

function stop_vpn {
    nmcli connection down $1\ \($USER\)
}

a vpndown="stop_vpn Stockholm || stop_vpn Uppsala || stop_vpn London"

a vpnupso="start_vpn Uppsala"
a vpnphlo="start_vpn Philadelphia"
a vpnstn="start_vpn Stockholm"
a vpnldn="start_vpn London"
a vpnbuh="start_vpn Bucharest"
a vpnman="start_vpn Manila"

a vpnstatus="nmcli con | grep -i vpn"

a vpnip="ip addr show dev tun0 | grep 'inet ' | cut -d ' ' -f 6 | sed -e s#/25##"
# #########################################
# Launch process on personal desktop
# #########################################
function remote {
    killall -9 google-chrome
    nohup google-chrome &

    killall -9 emacs
    nohup emacs &
}

function wpyinit {
    shopt -s dotglob
    cp -r ~/configuration/python/* .
    shopt -u dotglob
    mv Pipfile.work Pipfile
    rm Pipfile.personal
    mv env.work.rc .envrc
    rm env.personal.rc
}

alias heimdal='cp $HOME/.secrets/krb5.heimdal.conf /etc/krb5.conf'
alias mit='cp $HOME/.secrets/krb5.mit.conf /etc/krb5.conf'

function r3 {
    nohup `export QT_SCALE_FACTOR=1.4 && robo3t-snap` > /dev/null 2>&1 &
}

# Stops pip from hanging on "Loading macOS"
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

# Misc
function jtail {
    tail -f $1 | jq --unbuffered '.message'
}

# Datsci production
# kdp-app1-prod.stk.kambi.com
#export JAVA_HOME=$HOME/tools/java
#export PATH=$JAVA_HOME/bin:$PATH

function is_application {
    if [[ $1 == kambi* ]]; then
        return 1
    else
        return 0
    fi
}

function generate_keyfile {
    echo "Encryption types: aes256-cts-hmac-sha1-96"
    echo "                  arcfour-hmac-md5"
    /usr/bin/ktutil -k $HOME/.secrets/$USER.keytab add -p $USER@SERVICES.KAMBI.COM
}

# ###########################
# Forward sound over SSH
# ###########################
#if test "$SSH_CLIENT" ; then  pax11publish -i ;  export PULSE_SERVER=tcp:localhost:24713  ; fi
if test "$SSH_CLIENT" ; then export PULSE_SERVER=tcp:localhost:24713  ; fi

# Homebrew
export HOMEBREW_PREFIX="/home/linuxbrew/.linuxbrew";
export HOMEBREW_CELLAR="/home/linuxbrew/.linuxbrew/Cellar";
export HOMEBREW_REPOSITORY="/home/linuxbrew/.linuxbrew/Homebrew";
export PATH="/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin${PATH+:$PATH}";
export MANPATH="/home/linuxbrew/.linuxbrew/share/man${MANPATH+:$MANPATH}:";
export INFOPATH="/home/linuxbrew/.linuxbrew/share/info:${INFOPATH:-}";


# ASDF
source $HOME/.asdf/asdf.sh
source $HOME/.asdf/completions/asdf.bash
