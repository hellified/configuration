source ~/devel/common/bash/bash_common.sh

# s3-tree in npm is good to have

GetINISections ~/.aws/credentials

export AWS_DEFAULT_REGION=eu-north-1

function aws_local {
    export AWS_ACCESS_KEY_ID=${configuration_local["aws_access_key_id"]}
    export AWS_SECRET_ACCESS_KEY=${configuration_local["aws_secret_access_key"]}
    unset AWS_SESSION_TOKEN
    export AWS_PROFILE=local
}

function aws_sandbox {
    export AWS_ACCESS_KEY_ID=${configuration_sandbox["aws_access_key_id"]}
    export AWS_SECRET_ACCESS_KEY=${configuration_sandbox["aws_secret_access_key"]}
    unset AWS_SESSION_TOKEN
    export AWS_PROFILE=sandbox
}

function aws_test {
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    export AWS_PROFILE=dev-dataplatform
    #aws sso login
}

function aws_prod_lake {
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    export AWS_PROFILE=prod-datalake
    #aws sso login
}

function aws_prod_tooling {
    unset AWS_ACCESS_KEY_ID
    unset AWS_SECRET_ACCESS_KEY
    unset AWS_SESSION_TOKEN
    export AWS_PROFILE=prod-datatooling
    #aws sso login
}



function awsl {
    # aws s3api --endpoint-url http://localhost:4566 list-buckets --query "Buckets[].Name"

    echo "aws $1 --endpoint-url http://localhost:4566 ${@:2}"
    aws $1 --endpoint-url http://localhost:4566 "${@:2}"
}

# All files since a certain date
function select_s3_files() {
    S3_FOLDER=$1
    prev_run_date=$2
    echo "Previous run date: $prev_run_date"
    aws s3 ls --recursive $S3_FOLDER | awk -v prev="$prev_run_date" '$0 > prev {print $0}' | sort -n
}

# EKS setup
# aws eks update-kubeconfig --name dataplatform-eks

# Athena
function athena_query_on_db() {
    aws athena start-query-execution \
        --query-string  "$(cat $1)" \
        --query-execution-context Database=$2
}


alias aws_login="aws sso login"

# ################################
# Test EKS cluster Grafana
# ################################
# kubectl port-forward -n prometheus pod/prometheus-grafana-578cc9b78c-jd8b4 3000
# later, Chrome -> localhost:3000
# user/pass: admin/prom-operator
