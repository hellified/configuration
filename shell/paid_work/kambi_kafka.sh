# BEGIN: Production (Extended to AWS)
export PRODUCTION_BOOTSTRAP_SERVERS=kafka1.sda.kambi.com:9092,kafka2.sda.kambi.com:9092,kafka3.sda.kambi.com:9092,kafka4.sdb.kambi.com:9092,kafka5.sdb.kambi.com:9092
export PRODUCTION_ZOOKEEPERS=hdp-prod1-master4.sth.kambi.com,hdp-prod1-master5.sth.kambi.com,hdp-prod1-master6.sth.kambi.com
# END: Production (Extended to AWS)

# BEGIN: STK
export STK_BOOTSTRAP_SERVERS=kdp-kafka21-prod.stk.kambi.com:9092,kdp-kafka22-prod.stk.kambi.com:9092,kdp-kafka23-prod.stk.kambi.com:9092,kdp-kafka24-prod.stk.kambi.com:9092
export STK_ZOOKEPERS=kdp-zookeeper1-prod.stk.kambi.com:2181,kdp-zookeeper2-prod.stk.kambi.com:2181,kdp-zookeeper3-prod.stk.kambi.com:2181
# END: STK

# BEGIN: TEST42
export TEST42_BOOTSTRAP_SERVERS=kdp-kafka2-test42.dev.kambi.com:9093,kdp-kafka3-test42.dev.kambi.com:9093
export TEST42_ZOOKEEPERS=kdp-zookeeper1-test42.dev.kambi.com:2181,kdp-zookeeper2-test42.dev.kambi.com:2181,kdp-zookeeper3-test42.dev.kambi.com:2181

# KafkaClient {
#   org.apache.kafka.common.security.plain.PlainLoginModule required
#   serviceName="kafka"
#   username="kmb_kafka_s3_sink"
#   password="7%ca9+/hQ76^4x!<A8}weR";
# };
# END: TEST42

# BEGIN: Misc.
#export TEST_BOOTSTRAP_SERVERS=hdp-prod1-kafka7.sth.kambi.com:9092,hdp-prod1-kafka9.sth.kambi.com:9092,hdp-prod1-kafka10.sth.kambi.com:9092
export RELEASE_BOOTSTRAP_SERVERS=kafka1-release.sth.kambi.com:9092,kafka2-release.sth.kambi.com:9092,kafka3-release.sth.kambi.com:9092,kafka4-release.sth.kambi.com:9092,kafka5-release.sth.kambi.com:9092

export CLOUD_BOOTSTRAP_SERVERS=kafka1.sdc.kambi.com:9092,kafka2.sdc.kambi.com:9092,kafka3.sdc.kambi.com:9092,kafka4.sdc.kambi.com:9092,kafka5.sdc.kambi.com:9092
# END: Misc

export SYSTEM_ACCOUNT_KERBEROS_PRINCIPAL_FULL=${USER}
export SYSTEM_ACCOUNT_KERBEROS_KEYTAB_PATH=/home/${USER}/.secrets/${USER}.keytab

KAFKA_VERSION=2.7.2
SCALA_VERSION=2.13

bdp_source_box=hdp-gw01-prod.bdp.kambi.com

function prod_kafka {
    export kafka_env="prod"
}

function test_kafka {
    export kafka_env="test"
}

function stk_kafka {
    export kafka_env="stk"
}

function get_kafka_brokers {
    environment=$kafka_env
    if [[ "$environment" == "prod" ]]; then
        echo "$PRODUCTION_BOOTSTRAP_SERVERS"
    elif [[ "$environment" == "stk" ]]
    then
        echo "$STK_BOOTSTRAP_SERVERS"
    elif [[ "$environment" == "test" ]]
    then
         echo "$TEST42_BOOTSTRAP_SERVERS"
    else
        echo "UNKNOWN"
    fi
}

function get_zookeeper_quorum {
    environment=$kafka_env
    if [[ "$environment" == "prod" ]]
    then
        echo "$PRODUCTION_ZOOKEEPERS"
    elif [[ "$environment" == "stk" ]]
    then
        echo "$STK_ZOOKEPERS"
    elif [[ "$environment" == "test" ]]
    then
        echo "$TEST42_ZOOKEEPERS"
    else
        echo "UNKOWN"
    fi
}

function get_consumer_properties {
    environment=$kafka_env
    if [[ "$environment" == "prod" ]]
    then
        echo "$HOME/devel/kambi/conf/consumer.sdc.properties"
    elif [[ "$environment" == "stk" ]]
    then
        echo "$HOME/devel/kambi/conf/consumer.stk.properties"
    else
        echo "UNKOWN"
    fi
}

function refresh_bdp_env {
    rm -rf $TOOLS_DIR/old_env_files
    mv $TOOLS_DIR/env_files $TOOLS_DIR/old_env_files
    scp -r ${bdp_source_box}:/etc/bdp/conf/env_files $TOOLS_DIR
    chmod -R 777 $TOOLS_DIR/env_files
    cd $TOOLS_DIR/env_files
    sed -i 's/\/etc\/bdp\/conf\//\/home\/${USER}\/tools\//g' *
    source $TOOLS_DIR/env_files/kdp.kafka.hadoop.env
    sudo mkdir -p /etc/bdp/conf
    sudo ln -s $TOOLS_DIR/env_files /etc/bdp/conf/env_files
    sudo rm -rf /home/${USER}/devel/docker_files/kambi-jupyter/env_files
    cp -r $TOOLS_DIR/env_files /home/${USER}/devel/docker_files/kambi-jupyter
}
