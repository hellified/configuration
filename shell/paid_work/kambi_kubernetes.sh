source $HOME/devel/common/bash/bash_common.sh
source $HOME/.kubernetes.sh

function install_minikube {
    (
        cd /tmp
        sudo apt install qemu-kvm libvirt-daemon-system
        sudo usermod -aG libvirt $USER
        sudo usermod -aG kvm $USER

        echo "Modifications to [$USER] user have been made. You may need to log back in to see them."
        echo "Added groups `libvirt` and `kvm`."

        curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
        sudo install minikube-linux-amd64 /usr/local/bin/minikube
        rm minikube-linux-amd64

        sudo apt-get install cntlm
    )
}

function minikube_init {
    export AWS_ACCESS_KEY_ID=test
    export AWS_SECRET_ACCESS_KEY=test

    proxy_local
    cntlm.sh
}

function stop_minikube {
    #sthproxy

    minikube stop

    echo "==> Minikube status:"
    minikube status
}

function install_helm {
    curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -
    echo "deb https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
    sudo apt-get update
    sudo apt-get install helm
}

function local_airflow_forward {
    export POD_NAME=$(kubectl get pods --namespace airflow -l "component=webserver,tier=airflow" -o jsonpath="{.items[0].metadata.name}")
    echo "Airflow web pod: $POD_NAME"
    nohup kubectl port-forward --namespace airflow $POD_NAME 8080:8080 &
}

function helm_install_airflow {
    echo "Creating airflow namespace ..."
    kubectl create namespace airflow

    echo "Adding airflow helm repo..."
    helm repo add apache-airflow https://airflow.apache.org

    echo "Installing airflow on cluster ..."
    helm install airflow apache-airflow/airflow --namespace airflow

    # helm repo add airflow-stable https://airflow-helm.github.io/charts
    # helm repo update

    # kubectl create namespace airflow

    eval $(minikube docker-env)

    # export RELEASE_NAME=airflow-cluster
    # export NAMESPACE=airflow
    # export CHART_VERSION=8.4.1

    # helm install $RELEASE_NAME airflow-stable/airflow --namespace $NAMESPACE --version $CHART_VERSION

    secs=60
    echo "Pausing $secs seconds for startup"
    countdown $secs

    local_airflow_forward
}

function start_minikube {
    minikube_init

    export NO_PROXY="10.96.0.0/12,192.168.99.0/24,192.168.39.0/24,${NO_PROXY}"
    export no_proxy="10.96.0.0/12,192.168.99.0/24,192.168.39.0/24,${no_proxy}"
    nproxy

    minikube start --memory 6g --cpus 4 --driver=kvm2

    echo "==> List of minikube domains (there should be a minikube domain)"
    virsh list --all

    echo "==> List of minikube networks (there should be a default and a mk-minikube network)"
    virsh net-list

    echo "==> Minikube status:"
    minikube status

    echo ""
    echo "Switching docker to use minikube"
    eval $(minikube -p minikube docker-env)

    echo "Starting the dashboard"
    minikube dashboard &
}

function grafana_forward() {
    kubectl get pods -n prometheus | grep grafana | awk '{print $1}' | while read pod_name
    do
        (
            cd ~/tmp/
	        nohup kubectl port-forward --namespace prometheus "${pod_name}" 3000 &
        )
        echo "${pod_name} accessible at http://localhost:3000"
        echo "admin/prom-operator"
    done
}


alias airflow_pod_ls="kubectl get pods -n airflow"

function get_airflow_pod_name() {
    pod_pattern=$1
    echo $(kubectl get pods -n airflow | grep $pod_pattern | awk '{print $1}')
}

function airflow_bash {
    pod_name=$(kubectl get pods -n airflow | grep scheduler | awk '{print $1}')
    kubectl exec -it ${pod_name} -n airflow -- bash
}

function airflow_push {
    dag_file=$1
    kubectl get pods -n airflow | grep scheduler | awk '{print $1}' | while read pod_name
    do
        echo "Pushing [${dag_file}] to [${pod_name}]"
        kubectl -n airflow cp $dag_file ${pod_name}:/opt/airflow/dags/rico
    done
}

function airflow_remove {
    dag_file=$1
    kubectl get pods -n airflow | grep scheduler | awk '{print $1}' | while read pod_name
    do
        echo "Removing [${dag_file}] from [${pod_name}]"
        kubectl -n airflow exec ${pod_name} -- rm /opt/airflow/dags/rico/$dag_file
    done
}

function airflow_forward() {
    kubectl get pods -n airflow | grep web | awk '{print $1}' | while read pod_name
    do
    (
        cd ~/tmp/
	    nohup kubectl port-forward --namespace airflow ${pod_name} 8081:8080 &
    )
    done
    echo "${pod_name} accessible at http://localhost:8081"
}

function tail_json_pod {
    # Usage tail_json_pod <namespace> <pod pattern>
    namespace=$1
    pod_pattern=$2
    kubectl get pods -n ${namespace} | grep ${pod_pattern} | awk '{print $1}' | while read pod_name
    do
        kubectl -n ${namespace} logs -f ${pod_name} | jq | grep message | sed 's/"message": "//g' | sed 's/",//g'
    done
}


#alias klogs="kubectl logs -f $1 | tee $HOME/tmp/klog.log"

# kubectl exec -it airflow-scheduler-7c5f5d5555-7hjks -n airflow -- bash

function klogs {
    kubectl logs -f $1 | tee $HOME/tmp/klog.log
}

function awlog {
    kubectl logs -f -n airflow airflow-cluster-worker-0  | tee $HOME/tmp/airflow_worker.log
}

function killpods {
    kubectl delete pod,service $(kubectl get pods -n default | grep $1 | awk '{print $1}')
}

export KUBECTX_CURRENT_FGCOLOR=$(tput setaf 6) # blue text
export KUBECTX_CURRENT_BGCOLOR=$(tput setab 7) # white background

alias gcloud="docker run --rm gcr.io/google.com/cloudsdktool/cloud-sdk:latest gcloud"
source <(kubectl completion bash)

# kubectl -n yakt-test get pods
# kubectl -n yakt-test logs -f kafka-s3-sink-v2-driver
# kubectl -n yakt-test edit configmaps configmap-name
# kubectl -n yakt-test port-forward kafka-s3-sink-v2-driver 4040:4040
# kubectl -n yakt-test get externalsecrets,configmaps
