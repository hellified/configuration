#Pinkctl
function change_pinkctl_env() {
    if [ $# != 1 ]; then
        echo "Please specify environment:"
        echo " local, dev, stage, prod"
        return
    fi
    target=$1
    case "$target" in
        "local")
            echo "found local"
            export API_URL=http://localhost:8080/query
            ;;
        "stage")
            echo "found stage"
            echo "No api url configured"
            exit
            ;;

        "dev")
            echo "found dev"
            export API_URL=https://graphql.europe-west1.d.nhpro.se/query
            ;;
        *)
            echo "Unknown env: [${target}]"
            exit
            ;;
    esac
}

#source ~/.google_cloud.sh

export GOPRIVATE=github.com/AiflooAB

# Using google cloud sdk
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/isaac/work/nectarine/open_source/google-cloud-sdk/path.bash.inc' ]; then . '/home/isaac/work/nectarine/open_source/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/isaac/work/nectarine/open_source/google-cloud-sdk/completion.bash.inc' ]; then . '/home/isaac/work/nectarine/open_source/google-cloud-sdk/completion.bash.inc'; fi


# Use the new binary plugin for authentication instead of using the default provider-specific code
# https://cloud.google.com/blog/products/containers-kubernetes/kubectl-auth-changes-in-gke
export USE_GKE_GCLOUD_AUTH_PLUGIN=True


####################################
# BEGIN: Cloud SQL
####################################
function kill_cloud_proxy {
    proxy_pid=$(ps h -C cloud_sql_proxy -o pid)
    if [ -z ${proxy_pid+x} ]; then
        echo "Proxy is not running"
    else
        echo "Killing proxy running as PID: [${proxy_pid}]"
        kill -9 $proxy_pid
    fi

}

function get_env_region {
    if [ $# != 1 ]; then
        echo "Please specify environment"
        echo " example:"
        echo "    get_env_region stage"
        return
    fi

    sql_env=$1
    region=""

    case $sql_env in
        "dev")
            region="europe-west1"
            ;;
        "stage")
            region="us-central1"
            ;;
        "prod")
            region="us-central1"
            ;;
        *)
            echo "Unknow environment: [$sql_env]"
            exit
            ;;
    esac

    echo $region
}

function get_env_database {
    if [ $# != 1 ]; then
        echo "Please specify environment"
        echo " example:"
        echo "    get_env_database stage"
        return
    fi

    sql_env=$1
    database=""

    case $sql_env in
        "dev")
            database="ce-sql-983"
            ;;
        "stage")
            database="sql-stage-us-central1"
            ;;
        "prod")
            database="sql-prod-us-central1"
            ;;
        *)
            echo "Unknow environment: [$sql_env]"
            exit
            ;;
    esac

    echo $database
}

function get_env_project {
    if [ $# != 1 ]; then
        echo "Please specify environment"
        echo " example:"
        echo "    get_env_project stage"
        return
    fi

    sql_env=$1
    project=""
    case $sql_env in
        "dev")
            project="dev-europe-west1-200515"
            ;;
        "stage")
            project="stage-us-central1-201127"
            ;;
        "prod")
            project="prod-us-central1-210105"
            ;;
        *)
            "Unknown environment: [$sql_env]"
            exit
            ;;
    esac

    echo "$project"
}


function start_cloud_sql_proxy {
    if [ $# != 1 ]; then
        echo "Please specify environment"
        echo " example:"
        echo "    start_could_sql_proxy stage"
        return
    fi

    sql_env=$1
    project=$(get_env_project "$sql_env")
    region=$(get_env_region "$sql_env")
    database=$(get_env_database "$sql_env")

    kill_cloud_proxy
    echo "BEGIN: Starting [${sql_env}] cloud sql proxy: [${project}:${region}:${database}] ..."
    cloud_sql_proxy -instances=${project}:${region}:${database}=tcp:5432 &
    sleep 3
    echo "END: Starting [${sql_env}] cloud sql proxy: [${project}:${region}:${database}] ..."
}


function sql_promt_cloud {
    if [ $# != 2 ]; then
        echo "Please specify environment and app name"
        echo " example:"
        echo "    sql_prompt_cloud cloud-engine stage"
        return
    fi

    app=$1
    sql_env=$2
    start_cloud_sql_proxy $sql_env
    project=$(get_env_project "${sql_env}")
    PGPASSWORD=$(get_app_env_sql_password "${app}" "${sql_env}")\
              psql --host=localhost --port=5432 --username $app
}


function get_app_env_sql_password {
    if [ $# != 2 ]; then
        echo "Please specify environment and app name"
        echo " example:"
        echo "    get_app_env_sql_password cloud-engine stage"
        return
    fi

    app=$1
    sql_env=$2
    project=$(get_env_project "$sql_env")
    password=$(gcloud secrets versions access latest --secret sql_${app}_password --project ${project})
    echo $password
}

# ##################################
# BEGIN: Playground
# ##################################
function start_playground_cloud_sql_proxy {
    echo "Starting dev cloud sql proxy"
    killall -9 cloud_sql_proxy
    cloud_sql_proxy -instances=dev-europe-west1-200515:europe-west1:playground-isahod-dev=tcp:5432 &
}

function playground_sql {
    start_playground_cloud_sql_proxy
    sleep 5
    PGPASSWORD="mooky21" psql --host=localhost --port=5432 --username postgres
}

export CLOUDSQL_INSTANCE_NAME=playground-isahod-dev

# ##################################
# END: Playground
# ##################################
####################################
# END: Cloud SQL
####################################

# Prompt for nectarine ssh key password
eval $(keychain --eval --quiet nectarine)

function install_cloud_engine_dependency_chart {
    export HELM_RELEASE_NAME=dev-deps
    export CLOUDSQLPROXY_INSTANCECONNECTIONNAME=dev-europe-west1-200515:europe-west1:${CLOUDSQL_INSTANCE_NAME}
    export CLOUDSQLPROXY_SERVICEACCOUNTJSON=$HOME/.config/gcloud/application_default_credentials.json
    export ENFORCER_DATABASE_PASSWORD=$(pass google_cloud_platform/dev-europe-west1/sql/${CLOUDSQL_INSTANCE_NAME}/users/postgres)


    helm --debug install "${HELM_RELEASE_NAME}" ./deploy/helm/charts/dev-deps \
    --set-file  cfssl.configMapData.configJson=./configs/cfssl-dev/config.json \
    --set-file  cfssl.configMapData.csrCaJson=./configs/cfssl-dev/csr-ca.json \
    --set-file  cfssl.configMapData.csrVmqJson=./configs/cfssl-dev/csr-vmq.json \
    --set-file  cfssl.configMapData.makefile=./configs/cfssl-dev/Makefile \
    --set       enforcer.sqlProxy.instanceName="${CLOUDSQLPROXY_INSTANCECONNECTIONNAME}" \
    --set-file  enforcer.sqlproxySecretData.serviceAccountJson="${CLOUDSQLPROXY_SERVICEACCOUNTJSON}" \
    --set       enforcer.configSecretData.configToml.policyAdapter.database.username="postgres" \
    --set-file  enforcer.configSecretData.configToml.policyAdapter.database.password=<(printf "${ENFORCER_DATABASE_PASSWORD}") \
    --set       enforcer.existingConfigSecret="${HELM_RELEASE_NAME}-enforcer"
}

function install_cloud_engine_helm_chart() {
    export KAFKA_URL=dev-deps-cp-kafka:9092
    export MQTT_HOSTPORT=dev-deps-vernemq:1883
    export ENFORCER_ADDRESS=dev-deps-enforcer:5001
    export CFSSL_DEPLOYMENT_NAME=dev-deps-cfssl

    export APP_VERSION=2.43.0
    export DB_MIGRATE_IMAGE_TAG=stable
    export CLOUDSQLPROXY_INSTANCECONNECTIONNAME=dev-europe-west1-200515:europe-west1:${CLOUDSQL_INSTANCE_NAME}
    export GOOGLE_APPLICATION_CREDENTIALS=$HOME/.config/gcloud/application_default_credentials.json
    export DATABASE_PASSWORD=$(pass google_cloud_platform/dev-europe-west1/sql/${CLOUDSQL_INSTANCE_NAME}/users/postgres)
    export GRAPHQL_FQDN="graphql.$(minikube ip).nip.io"
    export TWOWAYAUDIO_FQDN="twowayaudio.$(minikube ip).nip.io"

    echo "$(minikube ip)    ${TWOWAYAUDIO_FQDN} ${GRAPHQL_FQDN}" > ${HOME}/.hosts
    
    export TWILIO_ACCOUNT_SID=junk
    export TWILIO_AUTH_TOKEN=junk
    # export TWILIO_ACCOUNT_SID=$(pass twilio/accounts/Nectarine_2way/subaccounts/nhpro-dev/account_sid)
    # export TWILIO_AUTH_TOKEN=$(pass twilio/accounts/Nectarine_2way/subaccounts/nhpro-dev/auth_token)

    export TWILIO_SIP_DOMAIN_SID=SDda326f55ddddf3a4de83a27053e25a18
    export TWILIO_CREDENTIAL_LIST_SID=CL23b4f57baeab3acc032067837ac022e5
    export TWILIO_FROM_NUMBER=+46105516488


    export BIGQUERY_INFIX=$(head -c 10 < /dev/urandom | base32)
    export CFSSL_KEY=$(kubectl --context=minikube exec "Deployment/${CFSSL_DEPLOYMENT_NAME}" -- cat /cfssl/conf/config.json | jq --raw-output '.auth_keys | .["ca-auth"].key')
    export CFSSL_CA_CERT=$(kubectl --context=minikube exec "Deployment/${CFSSL_DEPLOYMENT_NAME}" -- cat /cfssl/keys/ca.pem)
    export ENFORCER_TOKEN=$(yq eval '.enforcer.configSecretData.configToml.server.token' ./deploy/helm/charts/dev-deps/values.yaml)
    export SENDGRID_API_KEY=$(gcloud --project="dev-europe-west1-200515" secrets versions access latest --secret sendgrid_cloud-engine_dev-europe-west1)


    bin/helm --debug upgrade --install cloud-engine ./deploy/helm/charts/cloud-engine --version 0.0.0 \
             --set       cloudSqlProxy.instanceConnectionName="${CLOUDSQLPROXY_INSTANCECONNECTIONNAME}" \
             --set       dbMigrate.image.tag="${DB_MIGRATE_IMAGE_TAG}" \
             --set       dbMigrate.imagePullPolicy="IfNotPresent" \
             --set       dbMigrate.configMapData.postgresUser="postgres" \
             --set       dbMigrate.configMapData.postgresDatabase="cloud-engine" \
             --set-file  dbMigrate.secretData.googleApplicationCredentials="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set-file  dbMigrate.secretData.postgresPassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       testDb.configMapData.postgresUser="postgres" \
             --set       testDb.configMapData.postgresDatabase="cloud-engine" \
             --set-file  testDb.secretData.postgresPassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set-file  testDb.secretData.googleApplicationCredentials="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       global.imagePullSecrets="" \
             --set       deviceShadow.image.tag="${APP_VERSION}" \
             --set       deviceShadow.cmd.mainDatabaseUser="postgres" \
             --set       deviceShadow.cmd.mainDatabaseName="cloud-engine" \
             --set-file  deviceShadow.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       deviceShadow.cmd.mqttBrokerUri="tcp://device-shadow.services.int.nectarinehealth.com:@${MQTT_HOSTPORT}" \
             --set       deviceShadow.existingCertsSecret="" \
             --set       deviceShadow.existingGoogleCloudServiceAccountSecret="" \
             --set-file  deviceShadow.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       feednotification.image.tag="${APP_VERSION}" \
             --set       feednotification.cmd.mainDatabaseUser="postgres" \
             --set-file  feednotification.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       feednotification.cmd.mainDatabaseName="cloud-engine" \
             --set       feednotification.cmd.enforcerToken="${ENFORCER_TOKEN}" \
             --set       feednotification.existingGoogleCloudServiceAccountSecret="" \
             --set-file  feednotification.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       feednotification.kafka.url="${KAFKA_URL}" \
             --set       feednotificationreminder.image.tag="${APP_VERSION}" \
             --set       graphql.image.tag="${APP_VERSION}" \
             --set       graphql.ingress.fqdn="${GRAPHQL_FQDN}" \
             --set       graphql.cmd.mainDatabaseUser="postgres" \
             --set-file  graphql.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       graphql.cmd.mainDatabaseName="cloud-engine" \
             --set       graphql.cmd.cfsslHost="${CFSSL_DEPLOYMENT_NAME}" \
             --set       graphql.cmd.cfsslKey="${CFSSL_KEY}" \
             --set       graphql.cmd.enforcerToken="${ENFORCER_TOKEN}" \
             --set       graphql.cmd.sendgridApiKey="${SENDGRID_API_KEY}" \
             --set       graphql.existingGoogleCloudServiceAccountSecret="" \
             --set-file  graphql.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       graphql.existingNoomiCaSecret="" \
             --set       graphql.noomiCaSecretData.cacertsPem="${CFSSL_CA_CERT}" \
             --set       graphql.kafka.url="${KAFKA_URL}" \
             --set       hudson.image.tag="${APP_VERSION}" \
             --set       hudson.cmd.mainDatabaseUser="postgres" \
             --set-file  hudson.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       hudson.cmd.mainDatabaseName="cloud-engine" \
             --set       hudson.cmd.bigqueryAiEventsDataset="local_${BIGQUERY_INFIX}_aievents" \
             --set       hudson.cmd.bigquerySensorDataset="local_${BIGQUERY_INFIX}_sensor_data" \
             --set       hudson.existingGoogleCloudServiceAccountSecret="" \
             --set-file  hudson.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       hudson.kafka.url="${KAFKA_URL}" \
             --set       twowayaudio.image.tag="${APP_VERSION}" \
             --set       twowayaudio.ingress.fqdn="${TWOWAYAUDIO_FQDN}" \
             --set       twowayaudio.cmd.twilioApiUsername="${TWILIO_ACCOUNT_SID}" \
             --set       twowayaudio.cmd.twilioApiPassword="${TWILIO_AUTH_TOKEN}" \
             --set       twowayaudio.cmd.twilioPrimaryAuthToken="${TWILIO_AUTH_TOKEN}" \
             --set       twowayaudio.cmd.twilioAccountSid="${TWILIO_ACCOUNT_SID}" \
             --set       twowayaudio.cmd.twilioSipDomainSid="${TWILIO_SIP_DOMAIN_SID}" \
             --set       twowayaudio.cmd.twilioSipProxy="sip.de1.twilio.com" \
             --set       twowayaudio.cmd.twilioCredentialListSid="${TWILIO_CREDENTIAL_LIST_SID}" \
             --set       twowayaudio.cmd.twilioFromNumber="${TWILIO_FROM_NUMBER}" \
             --set       twowayaudio.cmd.mainDatabaseUser="postgres" \
             --set-file  twowayaudio.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       twowayaudio.cmd.mainDatabaseName="cloud-engine" \
             --set       twowayaudio.cmd.kafkaUrl="${KAFKA_URL}" \
             --set       twowayaudio.existingGoogleCloudServiceAccountSecret="" \
             --set-file  twowayaudio.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       watson.image.tag="${APP_VERSION}" \
             --set       watson.kafka.url="${KAFKA_URL}" \
             --set       watson.cmd.mainDatabaseUser="postgres" \
             --set-file  watson.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       watson.cmd.mainDatabaseName="cloud-engine" \
             --set       watson.existingGoogleCloudServiceAccountSecret="" \
             --set-file  watson.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}"
}

function render_helm_cloud {
    export KAFKA_URL=dev-deps-cp-kafka:9092
    export MQTT_HOSTPORT=dev-deps-vernemq:1883
    export ENFORCER_ADDRESS=dev-deps-enforcer:5001
    export CFSSL_DEPLOYMENT_NAME=dev-deps-cfssl

    export APP_VERSION=2.43.0
    export DB_MIGRATE_IMAGE_TAG=stable
    export CLOUDSQLPROXY_INSTANCECONNECTIONNAME=dev-europe-west1-200515:europe-west1:${CLOUDSQL_INSTANCE_NAME}
    export GOOGLE_APPLICATION_CREDENTIALS=$HOME/.config/gcloud/application_default_credentials.json
    export DATABASE_PASSWORD=$(pass google_cloud_platform/dev-europe-west1/sql/${CLOUDSQL_INSTANCE_NAME}/users/postgres)
    export GRAPHQL_FQDN="graphql.$(minikube ip).nip.io"
    export TWOWAYAUDIO_FQDN="twowayaudio.$(minikube ip).nip.io"
    export TWILIO_ACCOUNT_SID=junk
    export TWILIO_AUTH_TOKEN=junk
    # export TWILIO_ACCOUNT_SID=$(pass twilio/accounts/Nectarine_2way/subaccounts/nhpro-dev/account_sid)
    # export TWILIO_AUTH_TOKEN=$(pass twilio/accounts/Nectarine_2way/subaccounts/nhpro-dev/auth_token)

    export TWILIO_SIP_DOMAIN_SID=SDda326f55ddddf3a4de83a27053e25a18
    export TWILIO_CREDENTIAL_LIST_SID=CL23b4f57baeab3acc032067837ac022e5
    export TWILIO_FROM_NUMBER=+46105516488


    export BIGQUERY_INFIX=$(head -c 10 < /dev/urandom | base32)
    export CFSSL_KEY=$(kubectl --context=minikube exec "Deployment/${CFSSL_DEPLOYMENT_NAME}" -- cat /cfssl/conf/config.json | jq --raw-output '.auth_keys | .["ca-auth"].key')
    export CFSSL_CA_CERT=$(kubectl --context=minikube exec "Deployment/${CFSSL_DEPLOYMENT_NAME}" -- cat /cfssl/keys/ca.pem)
    export ENFORCER_TOKEN=$(yq eval '.enforcer.configSecretData.configToml.server.token' ./deploy/helm/charts/dev-deps/values.yaml)
    export SENDGRID_API_KEY=$(gcloud --project="dev-europe-west1-200515" secrets versions access latest --secret sendgrid_cloud-engine_dev-europe-west1)


    bin/helm template cloud-engine ./deploy/helm/charts/cloud-engine --version 0.0.0 \
             --set       cloudSqlProxy.instanceConnectionName="${CLOUDSQLPROXY_INSTANCECONNECTIONNAME}" \
             --set       dbMigrate.image.tag="${DB_MIGRATE_IMAGE_TAG}" \
             --set       dbMigrate.imagePullPolicy="IfNotPresent" \
             --set       dbMigrate.configMapData.postgresUser="postgres" \
             --set       dbMigrate.configMapData.postgresDatabase="cloud-engine" \
             --set-file  dbMigrate.secretData.googleApplicationCredentials="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set-file  dbMigrate.secretData.postgresPassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       testDb.configMapData.postgresUser="postgres" \
             --set       testDb.configMapData.postgresDatabase="cloud-engine" \
             --set-file  testDb.secretData.postgresPassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set-file  testDb.secretData.googleApplicationCredentials="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       global.imagePullSecrets="" \
             --set       deviceShadow.image.tag="${APP_VERSION}" \
             --set       deviceShadow.cmd.mainDatabaseUser="postgres" \
             --set       deviceShadow.cmd.mainDatabaseName="cloud-engine" \
             --set-file  deviceShadow.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       deviceShadow.cmd.mqttBrokerUri="tcp://device-shadow.services.int.nectarinehealth.com:@${MQTT_HOSTPORT}" \
             --set       deviceShadow.existingCertsSecret="" \
             --set       deviceShadow.existingGoogleCloudServiceAccountSecret="" \
             --set-file  deviceShadow.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       feednotification.image.tag="${APP_VERSION}" \
             --set       feednotification.cmd.mainDatabaseUser="postgres" \
             --set-file  feednotification.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       feednotification.cmd.mainDatabaseName="cloud-engine" \
             --set       feednotification.cmd.enforcerToken="${ENFORCER_TOKEN}" \
             --set       feednotification.existingGoogleCloudServiceAccountSecret="" \
             --set-file  feednotification.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       feednotification.kafka.url="${KAFKA_URL}" \
             --set       feednotificationreminder.image.tag="${APP_VERSION}" \
             --set       graphql.image.tag="${APP_VERSION}" \
             --set       graphql.ingress.fqdn="${GRAPHQL_FQDN}" \
             --set       graphql.cmd.mainDatabaseUser="postgres" \
             --set-file  graphql.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       graphql.cmd.mainDatabaseName="cloud-engine" \
             --set       graphql.cmd.cfsslHost="${CFSSL_DEPLOYMENT_NAME}" \
             --set       graphql.cmd.cfsslKey="${CFSSL_KEY}" \
             --set       graphql.cmd.enforcerToken="${ENFORCER_TOKEN}" \
             --set       graphql.cmd.sendgridApiKey="${SENDGRID_API_KEY}" \
             --set       graphql.existingGoogleCloudServiceAccountSecret="" \
             --set-file  graphql.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       graphql.existingNoomiCaSecret="" \
             --set       graphql.noomiCaSecretData.cacertsPem="${CFSSL_CA_CERT}" \
             --set       graphql.kafka.url="${KAFKA_URL}" \
             --set       hudson.image.tag="${APP_VERSION}" \
             --set       hudson.cmd.mainDatabaseUser="postgres" \
             --set-file  hudson.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       hudson.cmd.mainDatabaseName="cloud-engine" \
             --set       hudson.cmd.bigqueryAiEventsDataset="local_${BIGQUERY_INFIX}_aievents" \
             --set       hudson.cmd.bigquerySensorDataset="local_${BIGQUERY_INFIX}_sensor_data" \
             --set       hudson.existingGoogleCloudServiceAccountSecret="" \
             --set-file  hudson.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       hudson.kafka.url="${KAFKA_URL}" \
             --set       twowayaudio.image.tag="${APP_VERSION}" \
             --set       twowayaudio.ingress.fqdn="${TWOWAYAUDIO_FQDN}" \
             --set       twowayaudio.cmd.twilioApiUsername="${TWILIO_ACCOUNT_SID}" \
             --set       twowayaudio.cmd.twilioApiPassword="${TWILIO_AUTH_TOKEN}" \
             --set       twowayaudio.cmd.twilioPrimaryAuthToken="${TWILIO_AUTH_TOKEN}" \
             --set       twowayaudio.cmd.twilioAccountSid="${TWILIO_ACCOUNT_SID}" \
             --set       twowayaudio.cmd.twilioSipDomainSid="${TWILIO_SIP_DOMAIN_SID}" \
             --set       twowayaudio.cmd.twilioSipProxy="sip.de1.twilio.com" \
             --set       twowayaudio.cmd.twilioCredentialListSid="${TWILIO_CREDENTIAL_LIST_SID}" \
             --set       twowayaudio.cmd.twilioFromNumber="${TWILIO_FROM_NUMBER}" \
             --set       twowayaudio.cmd.mainDatabaseUser="postgres" \
             --set-file  twowayaudio.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       twowayaudio.cmd.mainDatabaseName="cloud-engine" \
             --set       twowayaudio.cmd.kafkaUrl="${KAFKA_URL}" \
             --set       twowayaudio.existingGoogleCloudServiceAccountSecret="" \
             --set-file  twowayaudio.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}" \
             --set       watson.image.tag="${APP_VERSION}" \
             --set       watson.kafka.url="${KAFKA_URL}" \
             --set       watson.cmd.mainDatabaseUser="postgres" \
             --set-file  watson.cmd.mainDatabasePassword=<(echo -n "${DATABASE_PASSWORD}") \
             --set       watson.cmd.mainDatabaseName="cloud-engine" \
             --set       watson.existingGoogleCloudServiceAccountSecret="" \
             --set-file  watson.googleCloudServiceAccountSecretData.keyJson="${GOOGLE_APPLICATION_CREDENTIALS}"

}
export TGH=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

# ##################################
# BEGIN: Kafka
# ##################################

#stage zookeeper: message-bus-cp-zookeeper-headless
#stage kafka: message-bus-cp-kafka-headless.default.svc.cluster.local
export PRODUCTION_BOOTSTRAP_SERVERS=message-bus-cp-kafka-0.message-bus-cp-kafka-headless.default:9092,message-bus-cp-kafka-1.message-bus-cp-kafka-headless.default:9092,message-bus-cp-kafka-2.message-bus-cp-kafka-headless.default:9092

export STAGE_BOOTSTRAP_SERVERS=message-bus-cp-kafka-0.message-bus-cp-kafka-headless.default:9092
export STAGE_ZOOKEEPERS=message-bus-cp-zookeeper-headless:2181

function kafka_proxy {
    kubectl port-forward --namespace default message-bus-cp-kafka-0 9092:9092
}

# ##################################
# END: Kafka
# ##################################

export SENDGRID_API_KEY='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'
