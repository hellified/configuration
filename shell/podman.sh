export DOCKER_HOST="unix://$XDG_RUNTIME_DIR/podman/podman.sock"

function psh() {
    podman exec -i -t  $@ sh -c "stty cols $COLUMNS rows $LINES && sh";
}

function pshi() {
    podman run --rm --entrypoint '/bin/sh' -it $@
}

function pbash() {
    podman exec -i -t  $@ bash -c "stty cols $COLUMNS rows $LINES && bash";
}

function pbashi() {
    podman run --rm --entrypoint '/bin/bash' -it $@
}

function pbashir() {
    podman run --rm --entrypoint '/bin/bash' -it -u root $@
}
