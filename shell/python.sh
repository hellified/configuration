# export WORKON_HOME=~/.venvs

export PIPENV_VENV_IN_PROJECT=true

if hash pipenv >/dev/null 2>&1
then
    eval "$(_PIPENV_COMPLETE=bash_source pipenv)"
    # eval "$(pipenv --completion)"
fi
# export PROJECT_HOME=$HOME/devel

export PYTHONDOCS=/usr/share/doc/python/html/

a pt="python -m unittest discover"
a pcv="coverage run  -m unittest discover; coverage html; firefox htmlcov/index.html"

export PATH=$PATH:~/.local/bin

function show_virtual_env {
  if [ -n "$VIRTUAL_ENV" ]; then
      # echo "($(basename $VIRTUAL_ENV))"
      echo "($(basename $(dirname $VIRTUAL_ENV)))"
  fi
}

export -f show_virtual_env
if [ -n PYTHON_PS1_SET ]; then
    PS1='$(show_virtual_env)'$PS1
    export PYTHON_PS1_SET=0
fi


export AIRFLOW_HOME=~/devel/.airflow

function pyinit {
    app_name=$1
    mkdir $app_name
    (
        cd $app_name
        shopt -s dotglob
        cp -r ~/configuration/python/* .
        shopt -u dotglob
        
        mv Pipfile.personal Pipfile
        rm Pipfile.*
        
        mv env.personal.rc .envrc
        rm env.*
        direnv allow
        
        pipenv install
        
        git init
        echo "README for $app_name" > README.md
        git add .
        git commit -m "Initial commit"

    )
}

function poinit {
    project_name=$1
    poetry new $project_name
    cd $project_name
    cp ~/configuration/python/poetry/env.poetry.rc .envrc
    cp ~/configuration/python/.gitignore .gitignore
    direnv allow
    poetry update pytest
    poetry add -D autopep8 better-exceptions black coverage elpy flake8 jedi pylint typing-extensions pytest-mock pytest-sugar pytest-watch
    echo "[tool.black]" >> pyproject.toml
    cd ..
}

# pip bash completion start
_pip_completion() {
    COMPREPLY=( $( COMP_WORDS="${COMP_WORDS[*]}" \
                   COMP_CWORD=$COMP_CWORD \
                   PIP_AUTO_COMPLETE=1 $1 ) )
}
complete -o default -F _pip_completion pip
# pip bash completion end

a ppack="python setup.py sdist bdist_wheel"
function psearch {
    pip3 search $1 | sort | grep $1
}

# Globally install package
gpip() {
    PIP_REQUIRE_VIRTUALENV="" pip "$@"
}

#source $HOME/configuration/python/django/django_bash_completion.sh
if [ -d $HOME/.poetry ]
then
    export PATH=$PATH:$HOME/.poetry/bin
fi

a ptw='ptw tests/ -- --last-failed --new-first --ignore-glob=".*#.*"'

a pymail='python -m smtpd -n -c DebuggingServer localhost:8025'

# Setting up pyenv
# the sed invocation inserts the lines at the start of the file
# after any initial comment lines
# export PYENV_ROOT="$HOME/.pyenv"
# export PATH="$PYENV_ROOT/bin:$PYENV_ROOT/shims:$PATH"
# eval "$(pyenv init -)"
