# source /usr/lib/rbenv/completions/rbenv.bash
# eval "$(rbenv init -)"
if [ "$HOSTNAME" = zeno ]; then
    export PATH=$PATH:/home/isaac/.gem/ruby/2.5.0/bin
fi

# Rails
a rmigrate="rails db:migrate; rails db:migrate RAILS_ENV=test"
a rrollback="rails db:rollback; rails db:rollback RAILS_ENV=test"

#Development
a trunk='cd ~/devel/nograde'
a lsd='rake data:sample'
a jtags='etags --declarations --language=java'
