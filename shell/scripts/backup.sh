#!/usr/bin/env bash

# #############################################
# 
# #############################################
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT
source $HOME/devel/common/bash/bash_common.sh

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

Script description here.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-c, --clone     Clone projects
-u, --update    Update local trunk of all projects
-l, --list      List all projects
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

# parse_params() {
#   # default values of variables set from params
#   flag=0
#   param=''

#   while :; do
#     case "${1-}" in
#     -h | --help) usage ;
#     -v | --verbose) set -x ;;
#     --no-color) NO_COLOR=1 ;;
#     -c | --clone) clone=1 ;;
#     -u | --update) update=1 ;;
#     -l | --list) list=1 ;;
#     -?*) die "Unknown option: $1" ;;
#     *) break ;;
#     esac
#     shift
#   done

#   args=("$@")
#   return 0
# }

# parse_params "$@"

source_directories=(
    $HOME/media/print/books/calibre
    $HOME/devel
    $HOME/writing
    $HOME/app_data
    $HOME/maps
    
)

if mountpoint -q /run/media/$USER/carbon; then
    source_directories+=(
        /run/media/$USER/carbon/media/images
        /run/media/$USER/carbon/work.bk
    )
else
    red "### Carbon not mounted, so skipping some backup dirs. ###"
fi

base_backup_dir="/run/media/$USER/yeti"

perform_backup(){
    if mountpoint -q $base_backup_dir; then
        for source_dir in "${source_directories[@]}"; do
            echo "Backing up: $source_dir"
            rsync -aP --delete $source_dir $base_backup_dir/backups
        done
    else
        red "### Backup directory is not mounted. ###"
        exit 1
    fi
}

# backup_machine="192.168.1.149"
# backup_machine_port="21022"

# backup_restore(){
#     if mountpoint -q $base_backup_dir; then
#         for source_dir in "${source_directories[@]}"; do
#             echo "Backing up: $source_dir"
#             rsync -aP --port 21022 --delete ${backup_machine}:${base_backup_dir}/backups/$(basename source_dir) /tmp/$source_dir 
#         done
#     else
#         red "### Backup directory is not mounted. ###"
#         exit 1
#     fi
# }


perform_backup
