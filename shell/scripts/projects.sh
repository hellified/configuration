#!/usr/bin/env bash

# #############################################
# 
# #############################################
set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT
source $HOME/devel/common/bash/bash_common.sh

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] [-f] -p param_value arg1 [arg2...]

Script description here.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-c, --clone     Clone projects
-u, --update    Update local trunk of all projects
-l, --list      List all projects
EOF
  exit
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

msg() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  msg "$msg"
  exit "$code"
}

parse_params() {
  # default values of variables set from params
  flag=0
  param=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 ;;
    -c | --clone) clone=1 ;;
    -u | --update) update=1 ;;
    -l | --list) list=1 ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")
  return 0
}

parse_params "$@"

organization="AiflooAB"

hosting_domain="github.com"

all_projects=(
 PushProx
 aifloo-nordic-sdk
 airstream
 ansible-prometheus
 backend
 bluetooth
 builder-base
 cloud-engine
 cloud-engine-builder
 cloud-platform
 common-go
 devops
 enforcer
 espa-gateway-reader
 firebase-auth-admin-cli
 flutter_blue
 gcs-proxy
 githubbackup
 go-kallax
 grafana-firebase-admin
 gruber
 hawkbit
 hawkbit-client
 hawkbitctl
 helm-gcs
 home-app
 home-flutter-components
 home_app_test_files
 insights-portal
 instopo
 lib12observe
 libaudit
 linphone-sdk
 meta-bc
 meta-rauc
 ml-recruitment
 node-proto
 noomi-fw-coding-guidelines
 noomi-node
 noomi-node-nordic-provisioning
 on-network-device
 opus
 orpheus-audio
 phonenumbers
 pinkctl
 portalctl
 prometheus-fping
 prometheus-mac-proxy
 protoactor-go
 protoportal
 protoportal-builder
 qa-setup
 remote-terminal
 send-welcome-email
 serial
 site-remote-access-services
 spark-insights
 spark-on-k8s-operator
 tensorflow
 tensorrt-grpc-go
 tinyprov
 vernemq
)

if [ -v clone ];then
    for project in "${all_projects[@]}"; do
        if [[ ! -d "${project}" ]]
        then
            green "Cloning [${project}]"
            git clone git@${hosting_domain}:${organization}/${project}.git
        else
            red "Directory [${project}] already exists."
        fi
    done
elif [ -v update ]; then
    for project in "${all_projects[@]}"; do
        green "Updating [${project}]"
        (
            
            cd $project
            branch=$(git remote show git@${hosting_domain}:${organization}/${project} | grep 'HEAD branch' | cut -d' ' -f5)
            git checkout $branch
            git pull
         )
    done
elif [ -v list ]; then
    echo  "Project listing"
    for project in "${all_projects[@]}"; do
        echo "${organization}/${project}"
    done
else
    usage
fi
