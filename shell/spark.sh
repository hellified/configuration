function install_spark {
    set -e
    rm -rf ~/tools/tmp/spark_install
    mkdir -p ~/tools/tmp/spark_install
    cd ~/tools/tmp/spark_install
    touch junk
    wget https://dlcdn.apache.org/spark/spark-3.2.1/spark-3.2.1-bin-hadoop3.2.tgz
    tar -xzf spark*tgz
    rm -rf ~/tools/spark
    rm -f spark*tgz
    ls
    mv *hadoop* spark
    ls
    mv spark ~/tools/         
}

export SPARK_HOME=~/tools/spark
export PATH=$PATH:$SPARK_HOME/bin
#. /etc/profile.d/apache-spark.sh
#export PATH=$PATH:/opt/apache-spark/bin

export PYTHONPATH="${PYTHONPATH}"
export PYSPARK_PYTHON="${PYTHONPATH}/python"
#export SPARK_HOME="/usr/hdp/current/spark2-client"
export PYLIB="$SPARK_HOME/python/lib"
export PYTHONPATH="$PYTHONPATH:$PYLIB/py4j-0.10.4-src.zip"
export PYTHONPATH="$PYTHONPATH:$PYLIB/pyspark.zip"
export PYSPARK_SUBMIT_ARGS="--name yarn pyspark-shell"
