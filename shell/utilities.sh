function setup_kubernetes {
    rm -rf ~/.kube
    mkdir ~/.kube
    ln -s ~/configuration/kubernetes/config   ~/.kube/config
    chmod 600 ~/.kube/config
}

function setup_elixir_emacs {
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "Setting up elixir language server"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    mkdir -p ~/tools/elixir
    (
        cd ~/tools/elixir
        git clone git@github.com:elixir-lsp/elixir-ls.git
        cd elixir-ls && mix deps.get && mix test
    )
}

function setup_clamav {
    yay -S clamav

    sudo freshclam

    sudo systemctl enable clamav-freshclam.service
    sudo systemctl start clamav-freshclam.service

    sudo systemctl enable clamav-daemon.service
    sudo systemctl start clamav-daemon.service

    echo "~~~~~~~~~~~~~~~~~~~~~~"
    echo "Performing test run..."
    echo "~~~~~~~~~~~~~~~~~~~~~~"
    curl https://secure.eicar.org/eicar.com.txt | clamscan -

    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "Installing more definitions..."
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    yay -S python-fangfrisch
    sudo -u clamav /usr/bin/fangfrisch --conf /etc/fangfrisch/fangfrisch.conf initdb

    sudo systemctl enable fangfrisch.timer
    sudo systemctl start fangfrisch.timer

    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    echo "Performing a scan..."
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    sudo clamscan --recursive --infected --exclude-dir='^/sys|^/dev' /
}


# function install_sdkman {
#     curl -s "https://get.sdkman.io" | bash
#     source "$HOME/.sdkman/bin/sdkman-init.sh"
#     sdk version
# }

function install_pyenv {
    curl -s "https://pyenv.run" | bash
}

function remind () {
  local COUNT="$#"
  local COMMAND="$1"
  local MESSAGE="$1"
  local OP="$2"
  shift 2
  local WHEN="$@"
  # Display help if no parameters or help command
  if [[ $COUNT -eq 0 || "$COMMAND" == "help" || "$COMMAND" == "--help" || "$COMMAND" == "-h" ]]; then
    echo "COMMAND"
    echo "    remind <message> <time>"
    echo "    remind <command>"
    echo
    echo "DESCRIPTION"
    echo "    Displays notification at specified time"
    echo
    echo "EXAMPLES"
    echo '    remind "Hi there" now'
    echo '    remind "Time to wake up" in 5 minutes'
    echo '    remind "Dinner" in 1 hour'
    echo '    remind "Take a break" at noon'
    echo '    remind "Are you ready?" at 13:00'
    echo '    remind list'
    echo '    remind clear'
    echo '    remind help'
    echo
    return
  fi
  # Check presence of AT command
  if ! which at >/dev/null; then
    echo "remind: AT utility is required but not installed on your system. Install it with your package manager of choice, for example 'sudo apt install at'."
    return
  fi
  # Run commands: list, clear
  if [[ $COUNT -eq 1 ]]; then
    if [[ "$COMMAND" == "list" ]]; then
      at -l
    elif [[ "$COMMAND" == "clear" ]]; then
      at -r $(atq | cut -f1)
    else
      echo "remind: unknown command $COMMAND. Type 'remind' without any parameters to see syntax."
    fi
    return
  fi
  # Determine time of notification
  if [[ "$OP" == "in" ]]; then
    local TIME="now + $WHEN"
  elif [[ "$OP" == "at" ]]; then
    local TIME="$WHEN"
  elif [[ "$OP" == "now" ]]; then
    local TIME="now"
  else
    echo "remind: invalid time operator $OP"
    return
  fi
  # Schedule the notification
  echo "notify-send '$MESSAGE' 'Reminder' -u critical" | at $TIME 2>/dev/null
  echo "Notification scheduled at $TIME"
}

spell() {
    local candidates oldifs word array_pos
    oldifs="$IFS"
    IFS=':'

    # Parse the apsell format and return a list of ":" separated words
    read -a candidates <<< "$(printf "%s\n" "$1" \
        | aspell -a \
        | awk -F':' '/^&/ {
            split($2, a, ",")
            result=""
            for (x in a) {
                gsub(/^[ \t]/, "", a[x])
                result = a[x] ":" result
            }
            gsub(/:$/, "", result)
            print result
        }')"

    # Reverse number and print the parsed bash array because the list comes
    # out of gawk backwards
    for item in "${candidates[@]}"; do
        printf '%s\n' "$item"
    done \
        | tac \
        | nl \
        | less -FirSX

    printf "[ $(tput setaf 2)?$(tput sgr0) ]\t%s" \
        'Enter the choice (empty to cancel, 0 for input): '
    read index

    [[ -z "$index" ]] && return
    [[  "$index" == 0 ]] && word="$1"

    [[ -z "$word" ]] && {
        array_pos=$(( ${#candidates[@]} - index ))
        word="${candidates[$array_pos]}"
    }

    [[ -n "$word" ]] && {
        printf "$word" | xsel -p
        printf "Copied '%s' to clipboard!\n" "$word"
    } || printf "[ $(tput setaf 1):($(tput sgr0) ] %s\n" 'No match found'


    IFS="$oldifs"
}
