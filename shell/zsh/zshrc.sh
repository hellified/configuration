HOSTNAME="$(hostname)"
HOSTNAME_SHORT="${HOSTNAME%%.*}"

# history
HISTFILE=~/.zsh_history
HISTSIZE=5000
SAVEHIST=5000

export PATH=$PATH:/usr/sbin:/sbin:$HOME/bin

setopt appendhistory autocd extendedglob 

# default apps
(( ${+BROWSER} )) || export BROWSER="w3m"
(( ${+PAGER} ))   || export PAGER="less"

# prompt (if running screen, show window #)
if [ x$WINDOW != x ]; then
    export PS1="$WINDOW:%~%# "
else
    #export PS1='%~ %# '
    export PS1='%~ %# '    
fi

# format titles for screen and rxvt
function title() {
  # escape '%' chars in $1, make nonprintables visible
  a=${(V)1//\%/\%\%}

  # Truncate command, and join lines.
  a=$(print -Pn "%40>...>$a" | tr -d "\n")

  case $TERM in
  screen)
    print -Pn "\ek$a:$3\e\\"      # screen title (in ^A")
    ;;
  xterm*|rxvt)
    print -Pn "\e]2;$2 | $a:$3\a" # plain xterm title
    ;;
  esac
}

source ~/.aliases.sh
source ~/.prompt.sh

source ~/.emacs.sh

source ~/.python.sh
source ~/.scala.sh
source ~/.java.sh

source ~/.docker.sh

source ~/.intergraph.sh

# colorful listings
zmodload -i zsh/complist
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

#emacs bindings
bindkey -e

autoload -U compinit
compinit

autoload -U promptinit
promptinit
###########################################
#Directory tracking inside emacs term
#
# chpwd() { print -P "\033AnSiTc %d" }

# print -P "\033AnSiTu %n"
# print -P "\033AnSiTc %d"
###########################################


### If you want zsh's completion to pick up new commands in $path automatically
### comment out the next line and un-comment the following 5 lines
#zstyle ':completion:::::' completer _complete _approximate
_force_rehash() {
  (( CURRENT == 1 )) && rehash
  return 1	# Because we didn't really complete anything
}
zstyle ':completion:::::' completer _force_rehash _complete _approximate

zstyle -e ':completion:*:approximate:*' max-errors 'reply=( $(( ($#PREFIX + $#SUFFIX) / 3 )) )'
zstyle ':completion:*:descriptions' format "- %d -"
zstyle ':completion:*:corrections' format "- %d - (errors %e})"
zstyle ':completion:*:default' list-prompt '%S%M matches%s'
zstyle ':completion:*' group-name ''
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.(^1*)' insert-sections true
zstyle ':completion:*' menu select
zstyle ':completion:*' verbose yes

#SSH hosts tab completion 
local _myhosts
_myhosts=( ${${${${(f)"$(<$HOME/.ssh/known_hosts)"}:#[0-9]*}%%\ *}%%,*} )
zstyle ':completion:*' hosts $_myhosts

export DE=xfce

#Getting rid of: QGtkStyle was unable to detect the current GTK+ theme
export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

# Run on new shell
have_fortune=`which fortune`
if [ -e $have_fortune ]; then
    echo ""
    fortune -a
    echo ""
fi

export MPD_HOST=palliate
export MPD_PORT=55066

#zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

export QT_STYLE_OVERRIDE=gtk

source `which activate.sh`

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"
