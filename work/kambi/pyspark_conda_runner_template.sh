#!/bin/bash -e
# TODO: Friendly usage info
# runner.sh <python backfill script> <base directory for topic> [<date dir to begin with>]

# source env files to setup python virtualenv environment
source /etc/bdp/conf/env_files/bdp.anaconda.env
source /etc/bdp/conf/env_files/bdp.pyspark.env
source /etc/bdp/conf/env_files/bdp.hadoop.env

# Adding green color to some lines in console to
# make things visually easier to track
NC='\033[0m' # No Color

GREEN='\033[0;32m'
function green() {
    printf "${GREEN}$@${NC}\n"
}

stamp=`date -Iseconds`
program_name=$1
filename=`basename $program_name .py`
base_dir=$2

PARSED_OPTIONS=$(getopt -n "$0"  -o s:t:b:e:  -- "$@")
eval set -- "$PARSED_OPTIONS"

while true;
do
  case "$1" in
    -b)
        begin_dir="dt=${2}"
        shift 2;;
    -e)
        end_dir="dt=${2}"
        shift 2;;
    -t)
        topic=${2}
        shift 2;;
    -s)
        schema=${2}
        shift 2;;
    --)
        shift
        break;;
  esac
done

# activate pyspark python virtualenv
source ${ANACONDA_BIN_DIR}/activate ${CURRENT_PYSPARK_ENV_FULLPATH}

declare -a month_array

echo "Base directory: ${base_dir}"
if [ ! -z $begin_dir ]; then
    echo "Begin processing with directory [${begin_dir}]"
fi

if [ ! -z $end_dir ]; then
    echo "End processing (inclusive) with directory [${end_dir}]"
fi

for dir_name in `hdfs dfs -ls -C $base_dir`
do
        tmp_month=`basename $dir_name | cut -d '-' -f -2`
        month_array+=($tmp_month)
done


unique_month_dirs=($(echo "${month_array[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
mkdir -p logs
for month_dir in "${unique_month_dirs[@]}"
do
    # echo "Begin dir: [${begin_dir}]"
    # echo "End dir: [${end_dir}]"
    # echo "Month dir: [${month_dir}]"
    if [ -z $begin_dir ] || [[ $month_dir > $begin_dir ]]  || [[ $month_dir == $begin_dir ]]; then
        if [ -z $end_dir ] || [[ $month_dir < $end_dir ]]  || [[ $month_dir == $end_dir ]]; then
            green "################################################################################"
            green "#             ${filename} for ${month_dir}"
            green "################################################################################"
            if [ -z $schema ]
            then
                python $program_name -t $topic -d $month_dir > >(tee -a logs/${filename}-${stamp}.stdout.log) 2> >(tee -a logs/${filename}-${stamp}.stderr.log >&2)
            else
                python $program_name -t $topic -s $schema -d $month_dir > >(tee -a logs/${filename}-${stamp}.stdout.log) 2> >(tee -a logs/${filename}-${stamp}.stderr.log >&2)
            fi
        fi
    fi
done

conda deactivate
