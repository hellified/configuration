#!/bin/bash -e

# ########################################################
# source env files to setup python virtualenv environment
# ########################################################
# This isn't using anaconda, but instead a locally installed
# spark. The reason is the use of the kambi libraries.
# They aren't installed in the anaconda environment at
# this time. Perhaps when it's more stable we can request it.
# ########################################################
source /etc/bdp/conf/env_files/bdp.anaconda.env
source /etc/bdp/conf/env_files/bdp.mongo.env
source /etc/bdp/conf/env_files/bdp.pyspark.env
source /etc/bdp/conf/env_files/bdp.hadoop.env

# ########################################################
# Allow use of platform native Hadoop libraries
# ########################################################
export LD_LIBRARY_PATH=$HADOOP_HOME/lib/native

# ########################################################
# Resolve issue with diff versions of protobuf
# ########################################################
export CLASSPATH=`$HADOOP_HOME/bin/hdfs classpath --glob`

# ########################################################
# Where do we keep the secrets?
# ########################################################
export APPLICATION_SECRETS_FILE=$HOME/.secrets/creds.ini

# ########################################################
# Adding green color to some lines in console to
# make things visually easier to track
# ########################################################
NC='\033[0m' # No Color

GREEN='\033[0;32m'
function green() {
    printf "${GREEN}$@${NC}\n"
}
# #######################################################

stamp=`date -Iseconds`

# Script
cmd_file="./generate_event_start.py"

# Config file
config_file='config.ini'
logging_config='logging.ini'

green "====> Generating historic event start features."

declare -A arguments

arguments['-c']="${config_file}"
arguments['-l']="${logging_config}"

for argument_name in "${!arguments[@]}"
do
    run_args+=" ${argument_name} ${arguments[$argument_name]}"
done

app_name="historic-eventStart"

mkdir -p logs

time pipenv run $cmd_file $run_args > >(tee -a logs/${app_name}-${stamp}.stdout.log) 2> >(tee -a logs/${app_name}-${stamp}.stderr.log >&2)

#conda deactivate
