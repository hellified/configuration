#!/usr/bin/env bash

# For each git directory
#  Get status for the directory.
#   Collect all statuses
#   If there are no uncommited local modifications
#     Pull from remote repo
#       If no conflicts
#         If there are commited/pushable mods
#            Push
#            Exit
#       Else: (conflicts)
#         List all git projects w/emphasis on conflicts
#         Exit
#   Else: (uncommited local mods)
#     List all git projects dirs w/emphasis on changes
#     Exit

# ?MODE: work vs. home (different directory paths)
